#pragma once

#include "primitives/LineGrid.hxx"
#include "primitives/Rect.hxx"

#include <vector>

class Cell
{
public:
    enum class State
    {
        Free,
        Busy,
    };
    enum class Owner
    {
        Snake,
        Feed,
        Nobody,
    };

    Cell()
        : mFree{ true }
        , mOwner{ Owner::Nobody } {};

    bool  IsFree() { return mFree; }
    Owner HasOwner() { return mOwner; }

    void MakeFree()
    {
        mFree  = true;
        mOwner = Owner::Nobody;
    }
    void MakeBusy(Owner owner)
    {
        mFree  = false;
        mOwner = owner;
    }

private:
    bool  mFree;
    Owner mOwner;
};

class Grid {
public:
    Grid(uint32_t density, uint32_t size, const glm::vec4 &color);

    uint32_t CellSize() const { return mCellSize; };

    uint32_t Density() const { return mDensity; }

    const glm::vec4 &Color() const { return mColor; }

    const glm::vec2 &Origin() const { return mOrigin; }

    Cell &GetCell(const glm::ivec2 &position) { return mState[position.x][position.y]; };

    const Cell &GetCell(const glm::ivec2 &position) const { return mState[position.x][position.y]; };

private:
    const uint32_t mDensity;

    uint32_t mCellSize;
    glm::vec2 mOrigin;
    glm::vec4 mColor;

    std::vector<std::vector<Cell>> mState;

    std::unique_ptr<Rect> mDrawingImage;
    std::unique_ptr<LineGrid> mLineOverlay;
};
