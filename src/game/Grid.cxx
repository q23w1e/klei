#include "Grid.hxx"

#include <algorithm>

Grid::Grid(uint32_t density, uint32_t size, const glm::vec4& color)
    : mDensity{ density }
    , mColor{ color }
{
    mCellSize = size / mDensity;

    mState.reserve(mDensity);
    for (auto i = 0; i < mDensity; ++i)
    {
        std::vector<Cell> row(mDensity);
        mState.emplace_back(row);
    }

    auto [width, height] = klei::Space::GetInstance().GetRender().WindowSize();
    mOrigin              = glm::vec2((width - size) * 0.5f, (height - size) * 0.5f);

    mDrawingImage = std::make_unique<Rect>(size, size, 20);

    mDrawingImage->mesh->style->SetParameter<glm::vec4>("uColor", color);
    mDrawingImage->position = glm::vec2(width * 0.5f, height * 0.5f);

    mLineOverlay = std::make_unique<LineGrid>(mDensity + 1, size / mDensity, size);

    mLineOverlay->position = mOrigin;
}
