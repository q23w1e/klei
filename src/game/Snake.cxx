#include "Snake.hxx"
#include "FeedWave.hxx"
#include "Game.hxx"
#include "Grid.hxx"
#include "SnakeInputComponent.hxx"
#include "primitives/LineCross.hxx"

#include <glm/gtc/random.hpp>

#include <Random.hxx>
#include <map>

Snake::Snake(uint32_t length)
    : mGrid{ Game::GetInstance().GetGrid() }
    , mUpdateRate{ 0.3f }
    , mInput{ new SnakeInputComponent(*this) }
{
    mGridCellSize = static_cast<float>(mGrid.CellSize());
    mGridDensity  = static_cast<float>(mGrid.Density());

    uint32_t startX = (mGrid.Density() / 2) - 1;
    uint32_t startY = 0;
    for (int i = 0; i < length; ++i)
    {
        auto piece    = generatePiece();
        auto position = glm::ivec2(startX, startY * -i);

        piece->gridPosition = position;
        mGrid.GetCell(position).MakeBusy(Cell::Owner::Snake);

        mPieces.emplace_back(std::move(piece));
    }
}

void Snake::Update(float deltaTime)
{
    mTimeSinceLastUpdate += deltaTime;
    if (mTimeSinceLastUpdate >= mUpdateRate)
    {
        auto nextDirection    = mInput->Direction();
        auto nextHeadPosition = mPieces.front()->gridPosition + nextDirection;

        updateIfGridBoundsExceeded(nextHeadPosition, nextDirection);

        if (auto target = mGrid.GetCell(nextHeadPosition); !target.IsFree())
        {
            switch (target.HasOwner())
            {
                case Cell::Owner::Snake:
                {
                    Game::GetInstance().GameOver();
                    break;
                }
                case Cell::Owner::Feed:
                {
                    mPieces.emplace_back(std::move(generatePiece()));
                    Game::GetInstance().Scored();
                    break;
                }
                default:
                    break;
            }
        }

        updatePieces();

        mPieces.front()->gridPosition = nextHeadPosition;
        mGrid.GetCell(nextHeadPosition).MakeBusy(Cell::Owner::Snake);

        mTimeSinceLastUpdate = 0;
    }
};

void Snake::updatePieces()
{
    mGrid.GetCell(mPieces.back()->gridPosition).MakeFree();
    for (auto i = mPieces.size() - 1; i > 0; --i)
    {
        mPieces[i]->gridPosition = mPieces[i - 1]->gridPosition;
    }
}

void Snake::updateIfGridBoundsExceeded(glm::ivec2& nextHeadPosition, const glm::ivec2& nextDirection)
{
    if (nextHeadPosition.x < 0 || nextHeadPosition.x >= mGridDensity)
    {
        nextHeadPosition.x -= nextDirection.x * static_cast<uint32_t>(mGridDensity);
    }
    else if (nextHeadPosition.y < 0 || nextHeadPosition.y >= mGridDensity)
    {
        nextHeadPosition.y -= nextDirection.y * static_cast<uint32_t>(mGridDensity);
    }
}

std::unique_ptr<Piece> Snake::generatePiece()
{
    auto color = glm::vec4(glm::vec3(klei::Random::FloatRange(0.3f, 0.7f)), 1.0);

    return std::make_unique<Piece>(mGridCellSize, color);
}
