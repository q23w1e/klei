#pragma once

#include "primitives/Rect.hxx"

class Beam : public Rect
{
public:
    explicit Beam(float speed, const glm::vec4 &color, uint32_t drawOrder);

    void Update(float deltaTime) override;

    void Activate();
    void Deactivate();

private:
    float mInitialSize;
    float mSpeed;
    float mLimit;
};
