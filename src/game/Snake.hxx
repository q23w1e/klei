#pragma once

#include "Beam.hxx"
#include "Grid.hxx"
#include "SnakeInputComponent.hxx"
#include "Piece.hxx"
#include "primitives/LineCross.hxx"

#include <core/Element.hxx>

#include <vector>

class Snake : public klei::Element
{
public:
    explicit Snake(uint32_t length = 2);

    void Update(float deltaTime) override;

private:
    void updatePieces();
    void updateIfGridBoundsExceeded(glm::ivec2& nextHeadPosition, const glm::ivec2& nextDirection);

    std::unique_ptr<Piece> generatePiece();

    SnakeInputComponent* mInput;

    Grid& mGrid;
    float mGridCellSize;
    float mGridDensity;

    float mUpdateRate;
    float mTimeSinceLastUpdate;

    std::vector<std::unique_ptr<Piece>> mPieces;
};
