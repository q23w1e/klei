#pragma once

#include "Grid.hxx"
#include "Piece.hxx"

#include <vector>

class FeedWave
{
public:
    explicit FeedWave(uint32_t count = 2);
    void Generate();

    std::vector<std::unique_ptr<Piece>> mFeed;

private:
    glm::ivec2 getFreeCell();
    Grid&     mGrid;

    uint32_t      mCount;
    glm::vec4 mColor;
};
