#pragma once

#include <core/Component.hxx>

#include <glm/vec2.hpp>

class SnakeInputHandler
{
public:
    SnakeInputHandler();
    virtual ~SnakeInputHandler() = default;

    virtual void Process() = 0;

    const glm::ivec2& Direction();

protected:
    void turnLeft();
    void turnRight();

    klei::Input& mInput;

    glm::ivec2 mDirection;
    glm::mat2  mRotation;
    int        mRotationGuard;
};

class KeyboardInputHandler : public SnakeInputHandler
{
public:
    void Process() override;
};

class TouchInputHandler : public SnakeInputHandler
{
public:
    void Process() override;

private:
    bool mTouchBegan = false;
};

class SnakeInputComponent : public klei::Component
{
public:
    explicit SnakeInputComponent(klei::Element& element);

    void Update(float deltaTime) override { mInputHandler->Process(); }

    const glm::ivec2& Direction() { return mInputHandler->Direction(); }

private:
    std::unique_ptr<SnakeInputHandler> mInputHandler;
};
