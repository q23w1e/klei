#include "Piece.hxx"
#include "Game.hxx"

Piece::Piece(float width, const glm::vec4& color, uint32_t drawOrder)
    : Rect(width, width, drawOrder)
    , gridPosition{ glm::vec2(0) }
{
    auto& grid   = Game::GetInstance().GetGrid();

    mGridCellSize = grid.CellSize();
    mGridOrigin   = grid.Origin();

    mesh->style->SetParameter<glm::vec4>("uColor", color);
}

void Piece::Update(float deltaTime)
{
    position = mGridOrigin + static_cast<glm::vec2>(gridPosition) * mGridCellSize + (mGridCellSize * 0.5f);
}
