#include "Beam.hxx"
#include "Game.hxx"

Beam::Beam(float speed, const glm::vec4& color, uint32_t drawOrder)
    : Rect(5, 5, drawOrder)
    , mInitialSize{ 5 }
    , mSpeed{ speed }
{
    auto& grid = Game::GetInstance().GetGrid();

    mLimit   = grid.CellSize() * grid.Density();
    position = grid.Origin() + glm::vec2(mLimit * 0.5f);

    mesh->style->SetParameter<glm::vec4>("uColor", color);
}

void Beam::Update(float deltaTime)
{
    scale += deltaTime * mSpeed;

    if (mInitialSize * scale.x > mLimit || mInitialSize * scale.y > mLimit)
    {
        Deactivate();
    }
}

void Beam::Activate()
{
    scale = glm::vec2(1);
    state = klei::Element::State::Active;
    mesh->Visibility(true);
}

void Beam::Deactivate()
{
    state = klei::Element::State::Inactive;
    mesh->Visibility(false);
}
