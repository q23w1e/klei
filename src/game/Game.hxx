#pragma once

#include "FeedWave.hxx"
#include "Grid.hxx"
#include "Snake.hxx"
#include "Timer.hxx"
#include "primitives/LineGrid.hxx"
#include "primitives/Rect.hxx"

#include <common/ABC.hxx>
#include <core/Element.hxx>
#include <core/Space.hxx>

#include <string>

class Game : public Singleton<Game>
{
public:
    void Init();
    void Destroy(){};

    bool IsPaused() { return mPaused; }
    void Pause() { mPaused = true; }

    Grid& GetGrid() { return *mGrid; }

    void Scored();
    void GameOver();

private:
    std::unique_ptr<Snake>     mSnake;
    std::unique_ptr<FeedWave>  mFeedWave;
    std::unique_ptr<Grid>      mGrid;
    std::unique_ptr<Beam>      mBeam;
    std::unique_ptr<LineCross> mCross;
    std::unique_ptr<Timer>     mTimer;

    bool mPaused = false;
};
