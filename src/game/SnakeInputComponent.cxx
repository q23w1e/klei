#include "SnakeInputComponent.hxx"

SnakeInputHandler::SnakeInputHandler()
    : mInput{ klei::Space::GetInstance().GetInput() }
    , mDirection{ glm::vec2(0, 1) }
    , mRotation{ glm::mat2(0, 1, -1, 0) }
    , mRotationGuard{ 0 }
{
}

void KeyboardInputHandler::Process()
{
    if (mInput.IsButtonPressed("LEFT"))
    {
        turnLeft();
    }
    else if (mInput.IsButtonPressed("RIGHT"))
    {
        turnRight();
    }
}

void TouchInputHandler::Process()
{
    auto& touch = mInput.GetTouch();
    if (mTouchBegan && touch.phase == klei::Touch::Phase::Ended)
    {
        (touch.position.x <= 0.5) ? turnLeft() : turnRight();
        mTouchBegan = false;
    }
    else if (touch.phase == klei::Touch::Phase::Began)
    {
        mTouchBegan = true;
    }
}

void SnakeInputHandler::turnLeft()
{
    if (mRotationGuard != 1)
    {
        mDirection = mRotation * mDirection;
        ++mRotationGuard;
    }
}

void SnakeInputHandler::turnRight()
{
    if (mRotationGuard != -1)
    {
        mDirection = -mRotation * mDirection;
        --mRotationGuard;
    }
}

const glm::ivec2& SnakeInputHandler::Direction()
{
    mRotationGuard = 0;

    return mDirection;
}

SnakeInputComponent::SnakeInputComponent(klei::Element& element)
    : klei::Component(element)
{
#ifdef __ANDROID__
    mInputHandler = std::make_unique<TouchInputHandler>();
#else
    mInputHandler = std::make_unique<KeyboardInputHandler>();
#endif
}
