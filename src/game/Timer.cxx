#include "Timer.hxx"
#include "Game.hxx"

void Timer::Update(float deltaTime)
{
    scale.x -= deltaTime * (1 / mTTL);
    if (scale.x <= 0)
    {
        Game::GetInstance().GameOver();
    }


    mTimeSinceLastUpdate += deltaTime;
    if (mTimeSinceLastUpdate >= mTimeScaleIncreaseRate)
    {
        auto& clock = klei::Space::GetInstance().GetClock();
        clock.TimeScale(clock.TimeScale() + 0.1f);
        mTimeSinceLastUpdate = 0;
    }
}
