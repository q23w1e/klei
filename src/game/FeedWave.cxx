#include "FeedWave.hxx"
#include "Game.hxx"

#include <cstdlib>
#include <glm/gtc/random.hpp>

#include <Random.hxx>

FeedWave::FeedWave(uint32_t count)
    : mGrid{ Game::GetInstance().GetGrid() }
    , mCount{ count }
{
    auto& grid = Game::GetInstance().GetGrid();

    mColor = grid.Color();
    for (int i = 0; i < mCount; ++i)
    {
        auto feed = std::make_unique<Piece>(grid.CellSize() - 1, mColor);
        mFeed.emplace_back(std::move(feed));
    }
}

void FeedWave::Generate()
{
    for (auto& feed : mFeed)
    {
        mGrid.GetCell(feed->gridPosition).MakeFree();

        feed->gridPosition = getFreeCell();

        mGrid.GetCell(feed->gridPosition).MakeBusy(Cell::Owner::Feed);
    }
}

glm::ivec2 FeedWave::getFreeCell()
{
    auto max      = mGrid.Density() - 1;
    auto position = static_cast<glm::ivec2>(klei::Random::IntRange(0, max), klei::Random::IntRange(0, max));

    return mGrid.GetCell(position).IsFree() ? position : getFreeCell();
}
