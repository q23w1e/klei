#pragma once

#include <core/Element.hxx>
#include <graphics/Geometry.hxx>
#include <graphics/MeshComponent.hxx>

class LineCross : public klei::Element
{
public:
    LineCross(float width, float height)
    {
        width *= 0.5f;
        height *= 0.5f;

        std::vector<glm::vec2> positions = {
            glm::vec2(-width, height),  // top left
            glm::vec2(width, -height),  // bottom right
            glm::vec2(-width, -height), // bottom left
            glm::vec2(width, height),   // top right
        };

        auto geometry = std::make_unique<klei::graphics::Geometry>(positions, klei::graphics::Geometry::Mode::LINES);
        auto style    = klei::graphics::Styles::FlatColor();
        mesh          = new klei::graphics::MeshComponent(*this, std::move(geometry), std::move(style), 1000);

        mesh->style->SetParameter<glm::vec4>("uColor", glm::vec4(1.0));
    }

    klei::graphics::MeshComponent* mesh;
};
