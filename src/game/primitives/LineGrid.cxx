#include "LineGrid.hxx"

LineGrid::LineGrid(uint32_t density, uint32_t offset, uint32_t length)
{
    std::vector<glm::vec2> positions{ 2 * 2 * density };
    for (uint32_t i = 0; i < density; ++i)
    {
        auto step            = i * offset;
        positions[i * 4]     = glm::vec2(step, 0);
        positions[i * 4 + 1] = glm::vec2(step, length);

        positions[i * 4 + 2] = glm::vec2(0, step);
        positions[i * 4 + 3] = glm::vec2(length, step);
    }

    auto geometry = std::make_unique<klei::graphics::Geometry>(positions, klei::graphics::Geometry::Mode::LINES);
    auto style    = klei::graphics::Styles::FlatColor();
    mesh          = new klei::graphics::MeshComponent(*this, std::move(geometry), std::move(style), 21);
}
