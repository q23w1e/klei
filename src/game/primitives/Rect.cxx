#include "Rect.hxx"

Rect::Rect(float width, float height, uint32_t drawOrder)
{
    auto geometry = klei::graphics::RectGeometry(width, height);
    auto style    = klei::graphics::Styles::FlatColor();
    mesh          = new klei::graphics::MeshComponent(*this, std::move(geometry), std::move(style), drawOrder);
}
