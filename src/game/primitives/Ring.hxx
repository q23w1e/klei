#pragma once

#include <core/Element.hxx>
#include <graphics/MeshComponent.hxx>

#include <algorithm>

class Ring : public klei::Element
{
public:
    Ring(float innerRadius, float outerRadius, uint32_t segments = 32);

    float InnerRadius() { return mInnerRadius; }
    void  InnerRadius(float radius);
    float OuterRadius() { return mOuterRadius; }
    void  OuterRadius(float radius);

private:
    float mInnerRadius;
    float mOuterRadius;
    uint32_t  mSegments;
};
