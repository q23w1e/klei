#pragma once

#include <core/Element.hxx>
#include <graphics/Geometry.hxx>
#include <graphics/MeshComponent.hxx>

#include <glm/vec2.hpp>

class LineGrid : public klei::Element
{
public:
    LineGrid(uint32_t density, uint32_t offset, uint32_t length);

    klei::graphics::MeshComponent* mesh;
};
