#pragma once

#include <core/Element.hxx>
#include <graphics/MeshComponent.hxx>

class Rect : public klei::Element
{
public:
    Rect(float width, float height, uint32_t drawOrder);

    klei::graphics::MeshComponent* mesh;
};
