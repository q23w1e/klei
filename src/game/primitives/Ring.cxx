#include "Ring.hxx"

Ring::Ring(float innerRadius, float outerRadius, uint32_t segments)
    : mInnerRadius{ innerRadius }
    , mOuterRadius{ outerRadius }
    , mSegments{ segments }
{
    auto mesh =
            new klei::graphics::MeshComponent(*this, klei::graphics::RingGeometry(mInnerRadius, mOuterRadius, segments),
                                              klei::graphics::Styles::FlatColor());

    mesh->style->SetParameter<glm::vec4>("uColor", glm::vec4(1.0));
}

void Ring::InnerRadius(float radius)
{
    if (auto mesh = GetComponentByType<klei::graphics::MeshComponent>())
    {
        float ratio = radius / mInnerRadius;
        auto  begin = std::begin(mesh->geometry->position->values);

        std::for_each(begin, begin + mSegments, [&](glm::vec2& position) { position *= ratio; });

        mInnerRadius = radius;
        mesh->geometry->position->NeedsUpdate();
    }
}

void Ring::OuterRadius(float radius)
{
    if (auto mesh = GetComponentByType<klei::graphics::MeshComponent>())
    {
        float ratio = radius / mOuterRadius;
        auto  begin = std::begin(mesh->geometry->position->values) + mSegments;
        auto  end   = std::end(mesh->geometry->position->values);

        std::for_each(begin, end, [&](glm::vec2& position) { position *= ratio; });

        mOuterRadius = radius;
        mesh->geometry->position->NeedsUpdate();
    }
}
