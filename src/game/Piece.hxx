#pragma once

#include "components/AABBComponent.hxx"
#include "primitives/Rect.hxx"

#include <glm/vec4.hpp>

class Piece : public Rect
{
public:
    Piece(float width, const glm::vec4& color, uint32_t drawOrder = 100);

    void Update(float deltaTime) override;

    glm::ivec2 gridPosition;

    static std::unique_ptr<Piece> create() { return std::make_unique<Piece>(50, glm::vec4(1.0)); }

private:
    float          mGridCellSize;
    glm::vec2      mGridOrigin;
};
