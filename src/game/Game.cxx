#include "Game.hxx"
#include "Beam.hxx"
#include "Piece.hxx"
#include "primitives/LineCross.hxx"
#include "primitives/Ring.hxx"

#include <core/Clock.hxx>

void Game::Init()
{
    auto& render = klei::Space::GetInstance().GetRender();
    render.ClearColor(0.88f);
    auto [width, height] = render.WindowSize();

    const uint32_t density  = 14;
    auto           gridSize = static_cast<uint32_t>(std::min(width, height) * 0.8);
    gridSize                = (gridSize / density) * density;

    mGrid     = std::make_unique<Grid>(density, gridSize, glm::vec4(glm::vec3(0.78f), 1.0f));
    mBeam     = std::make_unique<Beam>(95, glm::vec4(glm::vec3(1.f), 0.78f), 80);
    mFeedWave = std::make_unique<FeedWave>();
    mSnake    = std::make_unique<Snake>(4);

    mTimer = std::make_unique<Timer>(gridSize, 10, glm::vec4(1, 0, 0, 1));

    mTimer->position = mGrid->Origin();
    mTimer->position += glm::vec2(gridSize * 0.5f, gridSize + 15);

    mCross = std::make_unique<LineCross>(width, height);

    mCross->position = glm::vec2(width * 0.5f, height * 0.5f);
    mCross->mesh->Visibility(false);

    mFeedWave->Generate();
}

void Game::Scored()
{
    mFeedWave->Generate();
    mBeam->Activate();
    mTimer->scale.x = std::min(1.0f, mTimer->scale.x + 0.3f);
}

void Game::GameOver()
{
    // game over
    mCross->mesh->Visibility(true);
    klei::Space::GetInstance().GetClock().Paused(true);
}
