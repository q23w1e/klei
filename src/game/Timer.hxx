#pragma once

#include "primitives/Rect.hxx"

class Timer : public Rect
{
public:
    Timer(float width, float height, const glm::vec4& color)
        : Rect(width, height, 500)
        , mTimeScaleIncreaseRate{ 4.0f }
        , mTTL{ 10.f }
    {
        mesh->style->SetParameter<glm::vec4>("uColor", color);
    }

    void Update(float deltaTime) override;

private:
    const float mTimeScaleIncreaseRate;
    const float mTTL;

    float mTimeSinceLastUpdate;
};
