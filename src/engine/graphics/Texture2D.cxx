#include "Texture2D.hxx"

#include <iostream>
#include <sstream>

#include "../libraries/picopng/picopng.hxx"

namespace klei
{
namespace graphics
{

Texture2D::Texture2D(const std::string& filename, Texture::Format format, bool flipped)
    : mFormat{ format }
    , mFlipped{ flipped }
{
    auto image = loadFromFile(filename);
    if (image.empty())
    {
        std::stringstream error;
        error << "Failed to load texture. Provided path: " << filename;

        throw std::runtime_error(error.str());
    }

    glGenTextures(1, &mId);
    GLDebug::CHECK_ERROR();
    Bind();

    glTexImage2D(GL_TEXTURE_2D, 0, static_cast<int>(mFormat), mWidth, mHeight, 0, static_cast<int>(mFormat),
                 GL_UNSIGNED_BYTE, &image[0]);
    GLDebug::CHECK_ERROR();
    glGenerateMipmap(GL_TEXTURE_2D);
    GLDebug::CHECK_ERROR();

    UpdateParameters();

    glBindTexture(GL_TEXTURE_2D, 0);
    GLDebug::CHECK_ERROR();
};

std::vector<unsigned char> Texture2D::loadFromFile(const std::string& filename)
{
    std::vector<unsigned char> buffer, image;

    loadFile(buffer, filename);
    int error = decodePNG(image, mWidth, mHeight, buffer.empty() ? nullptr : &buffer[0], buffer.size(), false);
    if (error != 0)
    {
        std::cerr << "ERROR::TEXTURE::FAILED_TO_DECODE_PNG" << std::endl;
        image.clear();
    }

    return image;
}

void Texture2D::Bind()
{
    glBindTexture(GL_TEXTURE_2D, mId);
    GLDebug::CHECK_ERROR();
}

void Texture2D::Filter(Texture::Filter min, Texture::Filter mag) {}

void Texture2D::MipmapFilter(Texture::MipmapFilter min, Texture::MipmapFilter mag) {}

std::pair<Texture::Filter, Texture::Filter> Texture2D::Filter()
{
    return { mMinFilter, mMagFilter };
}

std::pair<Texture::MipmapFilter, Texture::MipmapFilter> Texture2D::MipmapFilter()
{
    return { mMinMipmapFilter, mMagMipmapFilter };
}

void Texture2D::UpdateParameters()
{

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, static_cast<int>(mMinFilter));
    GLDebug::CHECK_ERROR();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, static_cast<int>(mMagFilter));
    GLDebug::CHECK_ERROR();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, static_cast<int>(mMinMipmapFilter));
    GLDebug::CHECK_ERROR();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, static_cast<int>(mMagMipmapFilter));
    GLDebug::CHECK_ERROR();
}

} // namespace graphics
} // namespace klei
