#pragma once

#include "GLDebug.hxx"
#include "GLES3.hxx"

#include <cassert>
#include <string>
#include <memory>

namespace klei
{
namespace graphics
{

class ShaderStage
{
public:
    enum class Type : GLenum
    {
        VERTEX   = GL_VERTEX_SHADER,
        FRAGMENT = GL_FRAGMENT_SHADER
    };

    ShaderStage(const std::string& filename, Type type);
    ~ShaderStage();

    bool IsCompiled();

    GLuint Id() { return mId; }

private:
    std::string loadFromFile(const std::string& filename);

    Type   mType;
    GLuint mId;
};

} // namespace graphics
} // namespace klei
