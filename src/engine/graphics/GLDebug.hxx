#pragma once

#include <iostream>
#include <tuple>

#include "GLES3.hxx"

namespace klei
{

namespace GLDebug
{

GLenum CHECK_ERROR_(const char* file, uint32_t line);
#define CHECK_ERROR() CHECK_ERROR_(__FILE__, __LINE__);

namespace Program
{
using ActiveVarInfo = std::tuple<GLsizei, GLint, GLenum, std::string>;

ActiveVarInfo GetActiveAttr(GLuint program, GLuint index);
ActiveVarInfo GetActiveUniform(GLuint program, GLuint index);
} // namespace Program
} // namespace GLDebug

std::ostream& operator<<(std::ostream& out, const GLDebug::Program::ActiveVarInfo& varInfo);
} // namespace klei
