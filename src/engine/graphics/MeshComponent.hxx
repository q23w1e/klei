#pragma once

#include "Geometry.hxx"
#include "Shader.hxx"
#include "Style.hxx"
#include "common/ABC.hxx"
#include "core/Component.hxx"

#include <memory>

namespace klei
{
namespace graphics
{

class MeshComponent : public Component, public Drawable
{
public:
    MeshComponent(Element& element, std::unique_ptr<Geometry> geometry, std::unique_ptr<Style> style,
                  uint32_t drawOrder = 100);
    ~MeshComponent() override;

    void Update(float deltaTime) override;
    void Draw() override;

    std::unique_ptr<Geometry> geometry;
    std::unique_ptr<Style>    style;

};

} // namespace graphics
} // namespace klei
