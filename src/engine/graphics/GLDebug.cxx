#include "GLDebug.hxx"
#include "../common/ABC.hxx"

#include <iostream>
#include <sstream>
#include <utility>

#include "GLES3.hxx"

namespace klei
{
namespace GLDebug
{

GLenum CHECK_ERROR_(const char* fileName, uint32_t lineNumber)
{
    GLenum errorCode;
    while ((errorCode = glGetError()) != GL_NO_ERROR)
    {
        std::string error;
        // clang-format off
        switch (errorCode)
        {
            case GL_INVALID_ENUM: error = "INVALID_ENUM"; break;
            case GL_INVALID_VALUE: error = "INVALID_VALUE"; break;
            case GL_INVALID_OPERATION: error = "INVALID_OPERATION"; break;
            case GL_OUT_OF_MEMORY: error = "OUT_OF_MEMORY"; break;
            case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
            default: break;
        }
        // clang-format on
        std::cerr << error << " | " << fileName << ", " << lineNumber - 1 << std::endl;
    }

    return errorCode;
}

namespace Program
{
ActiveVarInfo GetActiveAttr(GLuint program, GLuint index)
{
    GLsizei length;
    GLenum  type;
    GLint   size;
    GLint   bufferSize = 0;
    glGetShaderiv(program, GL_INFO_LOG_LENGTH, &bufferSize);
    GLDebug::CHECK_ERROR();
    char name[bufferSize];

    glGetActiveAttrib(program, index, bufferSize, &length, &size, &type, name);
    CHECK_ERROR();

    return ActiveVarInfo{ length, size, type, name };
}

ActiveVarInfo GetActiveUniform(GLuint program, GLuint index)
{
    GLsizei length;
    GLenum  type;
    GLint   size;
    GLint   bufferSize = 0;
    glGetShaderiv(program, GL_INFO_LOG_LENGTH, &bufferSize);
    GLDebug::CHECK_ERROR();
    char name[bufferSize];

    glGetActiveUniform(program, index, bufferSize, &length, &size, &type, name);
    CHECK_ERROR();

    return ActiveVarInfo{ length, size, type, name };
}

} // namespace Program
} // namespace GLDebug

std::ostream& operator<<(std::ostream& out, const GLDebug::Program::ActiveVarInfo& varInfo)
{
    std::stringstream ss;
    ss << "Length: " << std::get<0>(varInfo) << " | ";
    ss << "Count: " << std::get<1>(varInfo) << " | ";
    ss << "Type: " << std::get<2>(varInfo) << " | ";
    ss << "Name: " << std::get<3>(varInfo);

    return out << ss.str();
}
} // namespace klei
