#pragma once

#include "GLES3.hxx"
#include "ShaderStage.hxx"

#include <iostream>

#include <glm/mat3x3.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

namespace klei
{
namespace graphics
{

class Shader
{
public:
    Shader(const std::string& vertFilePath, const std::string& fragFilePath);
    ~Shader();

    void Uniform1f(const std::string& name, float a);
    void Uniform3f(const std::string& name, const glm::vec3& v);
    void Uniform4f(const std::string& name, const glm::vec4& v);
    void UniformMatrix3fv(const std::string& name, const glm::mat3& m3);

    void Use();

    unsigned int ID() { return mId; }

private:
    unsigned int mId;

    std::unique_ptr<ShaderStage> mVertex;
    std::unique_ptr<ShaderStage> mFragment;
};

} // namespace graphics
} // namespace klei
