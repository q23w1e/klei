#include "ShaderStage.hxx"
#include "../common/Utils.hxx"

#include <sstream>

namespace klei
{
namespace graphics
{

ShaderStage::ShaderStage(const std::string& filename, Type type)
    : mType{ type }
{
    auto source = loadFromFile(filename);
    if (source.empty())
    {
        std::stringstream error;
        error << "Failed to load shader source file or file is empty. Provided path: " << filename << "\n";
        error << SDL_GetError();

        throw std::runtime_error(error.str());
    }
    const char* sourcePtr = source.data();

    mId = glCreateShader(static_cast<GLenum>(GetUnderlyingType(mType)));
    GLDebug::CHECK_ERROR();
    glShaderSource(mId, 1, &sourcePtr, nullptr);
    GLDebug::CHECK_ERROR();
    glCompileShader(mId);
    GLDebug::CHECK_ERROR();
}

std::string ShaderStage::loadFromFile(const std::string& filename)
{
    // SDL_RWclose can not be used as custom deleter for unique_ptr for unknown reasons (missed DECLSPEC?)
    auto file = SDL_RWFromFile(filename.c_str(), "rb");
    if (!file)
    {
        return "";
    }

    auto size  = static_cast<size_t>(SDL_RWsize(file));
    auto rdbuf = std::make_unique<char[]>(size + 1);

    for (int i = 0; i < size; ++i)
    {
        SDL_RWread(file, &rdbuf[i], sizeof(char), 1);
    }
    SDL_RWclose(file);

    rdbuf[size] = '\0';

    return std::move(std::string(rdbuf.get()));
}

bool ShaderStage::IsCompiled()
{
    GLint compiled;
    glGetShaderiv(mId, GL_COMPILE_STATUS, &compiled);

    if (!compiled)
    {
        GLint bufferSize = 0;
        glGetShaderiv(mId, GL_INFO_LOG_LENGTH, &bufferSize);
        GLDebug::CHECK_ERROR();
        char infoLog[bufferSize];
        glGetShaderInfoLog(mId, bufferSize, nullptr, infoLog);
        GLDebug::CHECK_ERROR();

        std::cerr << "ERROR::SHADER::COMPILATION_FAILED" << std::endl;
        std::cerr << infoLog << std::endl;

        return false;
    }

    return true;
}

ShaderStage::~ShaderStage()
{
    if (mId)
    {
        glDeleteShader(mId);
        GLDebug::CHECK_ERROR();
    }
}

} // namespace graphics
} // namespace klei
