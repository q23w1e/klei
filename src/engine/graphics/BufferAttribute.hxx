#pragma once

#include "GLDebug.hxx"
#include "common/Utils.hxx"

#include <string>
#include <vector>

namespace klei
{
namespace graphics
{

class Geometry;

enum class BufferType
{
    ARRAY_BUFFER         = GL_ARRAY_BUFFER,
    ELEMENT_ARRAY_BUFFER = GL_ELEMENT_ARRAY_BUFFER,
};

enum class BufferUsage
{
    STATIC_DRAW  = GL_STATIC_DRAW,
    DYNAMIC_DRAW = GL_DYNAMIC_DRAW,
};

template <typename T>
class BufferAttribute
{
public:
    BufferAttribute(const std::string& name, std::vector<T> values, BufferType type = BufferType::ARRAY_BUFFER);
    ~BufferAttribute();

    void Bind();
    void Unbind();
    void NeedsUpdate();
    void NeedsResend();

    GLuint   Id() { return mId; };
    size_t   Count() { return values.size(); };
    size_t   CountPerElement();
    uint32_t SizePerElement();
    uint32_t Size() { return static_cast<uint32_t>(Count()) * SizePerElement(); }

    std::string    name;
    std::vector<T> values;

    friend class Geometry;

private:
    void updateData();
    void resendData();

    GLuint      mId;
    BufferUsage mUsage;
    BufferType  mType;

    bool   mNeedsUpdate;
    bool   mNeedsResend;
    size_t mLastSentSize;
};

template <typename T>
BufferAttribute<T>::BufferAttribute(const std::string& name, std::vector<T> values, BufferType type)
    : name{ name }
    , values{ values }
    , mType{ type }
{
    mUsage = BufferUsage::STATIC_DRAW;
    glGenBuffers(1, &mId);
    GLDebug::CHECK_ERROR();
    resendData();
}

template <typename T>
BufferAttribute<T>::~BufferAttribute()
{
    if (mId)
    {
        glDeleteBuffers(1, &mId);
        GLDebug::CHECK_ERROR();
    }
}

template <typename T>
void BufferAttribute<T>::resendData()
{
    mNeedsResend = false;

    if (mId)
    {
        Bind();
        GLDebug::CHECK_ERROR();
        glBufferData(GetUnderlyingType(mType), Size(), values.data(), GetUnderlyingType(mUsage));
        GLDebug::CHECK_ERROR();
        Unbind();

        mLastSentSize = Count();
    }
}

template <typename T>
void BufferAttribute<T>::updateData()
{
    mNeedsUpdate = false;

    if (mId)
    {
        if (mLastSentSize != Count())
        {
            std::cout << "The size mismatch with previously sent data. Consider to use NeedsResend()" << std::endl;
            return;
        }

        Bind();
        glBufferSubData(GetUnderlyingType(mType), 0, Size(), values.data());
        GLDebug::CHECK_ERROR();
        Unbind();
    }
}

template <typename T>
void BufferAttribute<T>::Bind()
{
    if (mId)
    {
        glBindBuffer(GetUnderlyingType(mType), mId);
        GLDebug::CHECK_ERROR();
    }
}

template <typename T>
void BufferAttribute<T>::Unbind()
{
    glBindBuffer(GetUnderlyingType(mType), 0);
    GLDebug::CHECK_ERROR();
}

template <typename T>
void BufferAttribute<T>::NeedsUpdate()
{
    updateData();
}

template <typename T>
void BufferAttribute<T>::NeedsResend()
{
    resendData();
}

template <typename T>
uint32_t BufferAttribute<T>::SizePerElement()
{
    if (!values.empty())
    {
        return sizeof(values.front());
    }

    return 0;
}

template <typename T>
size_t BufferAttribute<T>::CountPerElement()
{
    if (!values.empty())
    {
        return values.front().length();
    }

    return 0;
}

} // namespace graphics
} // namespace klei
