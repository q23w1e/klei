#include "Shader.hxx"
#include "../core/Space.hxx"

#include <fstream>
#include <sstream>
#include <string>

#include <glm/vec3.hpp>

namespace klei
{
namespace graphics
{

Shader::Shader(const std::string& vertFilePath, const std::string& fragFilePath)
{
    mVertex   = std::make_unique<ShaderStage>(vertFilePath, ShaderStage::Type::VERTEX);
    mFragment = std::make_unique<ShaderStage>(fragFilePath, ShaderStage::Type::FRAGMENT);

    if (mVertex->IsCompiled() && mFragment->IsCompiled())
    {
        mId = glCreateProgram();
        GLDebug::CHECK_ERROR();
        glAttachShader(mId, mVertex->Id());
        GLDebug::CHECK_ERROR();
        glAttachShader(mId, mFragment->Id());
        GLDebug::CHECK_ERROR();
        glLinkProgram(mId);
        GLDebug::CHECK_ERROR();
    }
    else
    {
        std::cout << "ERROR::PROGRAM_CREATION_SKIPPED" << std::endl;
    }

    auto render = Space::GetInstance().GetRender();
}

Shader::~Shader()
{
    glDeleteProgram(mId);
}

void Shader::Use()
{
    if (!mId)
    {
        std::cout << "Program Id is 0. Use binding skipped." << std::endl;
        return;
    }

    glUseProgram(mId);
    GLDebug::CHECK_ERROR();
}

void Shader::Uniform3f(const std::string& name, const glm::vec3& v)
{
    GLint location = glGetUniformLocation(mId, name.c_str());
    GLDebug::CHECK_ERROR();
    if (location == -1)
    {
        std::cerr << "ERROR::PROGRAM::UNIFORM_NOT_FOUND: " << name << std::endl;
        return;
    }

    glUniform3f(location, v.x, v.y, v.z);
    GLDebug::CHECK_ERROR();
}

void Shader::Uniform4f(const std::string& name, const glm::vec4& v)
{
    GLint location = glGetUniformLocation(mId, name.c_str());
    GLDebug::CHECK_ERROR();
    if (location == -1)
    {
        std::cerr << "ERROR::PROGRAM::UNIFORM_NOT_FOUND: " << name << std::endl;
        return;
    }

    glUniform4f(location, v.r, v.g, v.b, v.a);
    GLDebug::CHECK_ERROR();
}

void Shader::Uniform1f(const std::string& name, float a)
{
    GLint location = glGetUniformLocation(mId, name.c_str());
    GLDebug::CHECK_ERROR();
    if (location == -1)
    {
        std::cerr << "ERROR::PROGRAM::UNIFORM_NOT_FOUND: " << name << std::endl;
        return;
    }

    glUniform1f(location, a);
    GLDebug::CHECK_ERROR();
}

void Shader::UniformMatrix3fv(const std::string& name, const glm::mat3& m3)
{
    GLint location = glGetUniformLocation(mId, name.c_str());
    GLDebug::CHECK_ERROR();
    if (location == -1)
    {
        std::cerr << "ERROR::PROGRAM::UNIFORM_NOT_FOUND: " << name << std::endl;
        return;
    }
    glUniformMatrix3fv(location, 1, GL_FALSE, &m3[0][0]);
    GLDebug::CHECK_ERROR();
}

} // namespace graphics
} // namespace klei
