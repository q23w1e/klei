#pragma once

#include "../common/ABC.hxx"
#include "../core/Element.hxx"
#include "Shader.hxx"

#include <map>
#include <variant>

#include <common/Storage.hxx>
#include <glm/mat3x3.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

namespace klei
{
namespace graphics
{

class Style
{
public:
    using Parameter = std::variant<float, glm::vec2, glm::vec3, glm::vec4, glm::mat3>;

    explicit Style(std::unique_ptr<Shader> shader);

    void UpdateMVPMatrix(const glm::mat3& model);
    void UpdateUniforms();
    void Use();

    template <typename T>
    T* GetParameter(const std::string& name)
    {
        if (mParameters.count(name))
        {
            try
            {
                return &std::get<T>(mParameters[name]);
            }
            catch (std::bad_variant_access&)
            {
                return nullptr;
            }
        }

        return nullptr;
    }

    template <typename T>
    void SetParameter(const std::string& name, T value)
    {
        mParameters[name] = std::move(value);
    }

protected:
    std::unique_ptr<Shader>          mShader;
    std::map<std::string, Parameter> mParameters;

    glm::mat3 mModelViewProjectionMatrix;
};

class Styles
{
public:
    static std::unique_ptr<Style> FlatColor();
    static std::unique_ptr<Style> VertexColor();
};

}; // namespace graphics
} // namespace klei
