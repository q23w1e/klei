#pragma once

#include "BufferAttribute.hxx"
#include "GLDebug.hxx"

#include <memory>
#include <vector>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace klei
{
namespace graphics
{

class Geometry
{
public:
    enum class Mode : GLenum
    {
        TRIANGLES     = GL_TRIANGLES,
        LINES         = GL_LINES,
        TRIANGLES_FAN = GL_TRIANGLE_FAN,
    };

    Geometry(std::vector<glm::vec2> positions, std::vector<glm::ivec3> indices, Mode mode = Mode::TRIANGLES);
    Geometry(std::vector<glm::vec2> positions, Mode mode = Mode::LINES);
    ~Geometry();

    std::unique_ptr<BufferAttribute<glm::vec2>> position;
    //    std::unique_ptr<BufferAttribute<glm::vec2>>  color;
    //    std::unique_ptr<BufferAttribute<glm::vec2>>  normal;
    std::unique_ptr<BufferAttribute<glm::ivec3>> index;

    void DrawElements();
    void UpdateBuffersIfNeeded();

private:
    void setup();
    void bindVAO();
    void unbindVAO();

    GLuint mVAO;
    Mode   mMode;
};

std::unique_ptr<Geometry> RectGeometry(float width, float height, Geometry::Mode mode = Geometry::Mode::TRIANGLES);
std::unique_ptr<Geometry> CircleGeometry(float radius, uint32_t segments = 32);
std::unique_ptr<Geometry> RingGeometry(float innerRadius, float outerRadius, uint32_t segments = 32);

}; // namespace graphics
} // namespace klei
