#pragma once

#include "../common/ABC.hxx"
#include "GLES3.hxx"

#include <memory>
#include <vector>

#include <glm/mat3x3.hpp>
#include <glm/vec4.hpp>

namespace klei
{
namespace graphics
{

class Render final : public Subsystem
{
public:
    std::string Init() override;
    void        Destroy() override;

    void ClearBuffer();
    void SwapBuffers();
    void Update();
    void AddToQueue(Drawable* drawable);
    void RemoveFromQueue(Drawable* drawable);

    const glm::vec4& ClearColor() const { return mClearColor; };
    void             ClearColor(const glm::vec4& color) { mClearColor = color; };
    void             ClearColor(float c) { mClearColor = glm::vec4(c, c, c, 1.0f); }

    std::pair<int, int> WindowSize();

    const glm::mat3& ProjectionMatrix() const { return mProjectionMatrix; };
    const glm::mat3& ViewMatrix() const { return mViewMatrix; };

private:
    void updateProjectionMatrix();
    void updateViewMatrix();

    SDL_Window*   mWindow;
    SDL_GLContext mContext;

    glm::vec4 mClearColor;

    glm::mat3 mProjectionMatrix;
    glm::mat3 mViewMatrix;

    std::vector<Drawable*> mDrawables;
};

} // namespace graphics
} // namespace klei
