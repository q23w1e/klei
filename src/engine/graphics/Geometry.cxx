#include "Geometry.hxx"

#include <cmath>

namespace klei
{
namespace graphics
{

Geometry::Geometry(std::vector<glm::vec2> positions, std::vector<glm::ivec3> indices, Mode mode)
    : mMode{ mode }
{
    this->position =
        std::make_unique<BufferAttribute<glm::vec2>>("position", std::move(positions), BufferType::ARRAY_BUFFER);
    this->index =
        std::make_unique<BufferAttribute<glm::ivec3>>("index", std::move(indices), BufferType::ELEMENT_ARRAY_BUFFER);

    glGenVertexArrays(1, &mVAO);
    GLDebug::CHECK_ERROR();

    setup();
}

Geometry::Geometry(std::vector<glm::vec2> positions, Mode mode)
    : mMode{ mode }
{
    this->position =
        std::make_unique<BufferAttribute<glm::vec2>>("position", std::move(positions), BufferType::ARRAY_BUFFER);
    this->index = nullptr;

    glGenVertexArrays(1, &mVAO);
    GLDebug::CHECK_ERROR();

    setup();
}

void Geometry::setup()
{
    bindVAO();

    position->Bind();

    glVertexAttribPointer(0, static_cast<GLint>(position->CountPerElement()), GL_FLOAT, GL_FALSE,
                          position->SizePerElement(), nullptr);
    GLDebug::CHECK_ERROR();
    glEnableVertexAttribArray(0);
    GLDebug::CHECK_ERROR();

    unbindVAO();
}

void Geometry::bindVAO()
{
    if (mVAO)
    {
        glBindVertexArray(mVAO);
        GLDebug::CHECK_ERROR();
    }
}

void Geometry::unbindVAO()
{
    glBindVertexArray(0);
    GLDebug::CHECK_ERROR();
}

void Geometry::DrawElements()
{

    bindVAO();
    if (index != nullptr)
    {
        index->Bind();
        glDrawElements(GetUnderlyingType(mMode), static_cast<GLsizei>(index->Count() * index->CountPerElement()),
                       GL_UNSIGNED_INT, nullptr);
    }
    else
    {
        glDrawArrays(GetUnderlyingType(mMode), 0, position->Size());
    }
    GLDebug::CHECK_ERROR();
    unbindVAO();
}

void Geometry::UpdateBuffersIfNeeded()
{
    if (position->mNeedsUpdate)
    {
        position->updateData();
    }

    if (index != nullptr && index->mNeedsUpdate)
    {
        index->updateData();
    }
}

Geometry::~Geometry()
{
    if (mVAO)
    {
        glDeleteVertexArrays(1, &mVAO);
        GLDebug::CHECK_ERROR();
    }
}

// clang-format off
const std::vector<glm::vec2> vertices = {
        glm::vec2(0.5f,  0.5f),// top right
        glm::vec2(0.5f, -0.5f),// bottom right
        glm::vec2(-0.5f, -0.5f), // bottom left
        glm::vec2(-0.5f,  0.5f), // top left
};
const std::vector<glm::ivec3> indices = {  // note that we start from 0!
        glm::ivec3(0, 1, 3),   // first triangle
        glm::ivec3(1, 2, 3)   // second triangle
};
const std::vector<glm::vec3> colors = {
        glm::vec3(1.0f, 1.0f, 1.0f),
        glm::vec3(1.0f, 1.0f, 1.0f),
        glm::vec3(1.0f, 1.0f, 1.0f),
        glm::vec3(1.0f, 1.0f, 1.0f),
};
const std::vector<glm::vec2> uvs = {
        glm::vec2(0.0f, 1.0f),// top right
        glm::vec2(0.0f, 0.0f),// bottom right
        glm::vec2(1.0f, 0.0f), // bottom left
        glm::vec2(1.0f, 1.0f), // top left
};
// clang-format on

std::unique_ptr<Geometry> RectGeometry(float width, float height, Geometry::Mode mode)
{
    width *= 0.5f;
    height *= 0.5f;

    std::vector<glm::vec2> positions = {
        glm::vec2(width, height),   // top right
        glm::vec2(width, -height),  // bottom right
        glm::vec2(-width, -height), // bottom left
        glm::vec2(-width, height),  // top left
    };

    return std::make_unique<Geometry>(positions, indices, mode);
}

std::unique_ptr<Geometry> CircleGeometry(float radius, uint32_t segments)
{
    std::vector<glm::vec2>  positions{ segments + 1 };
    std::vector<glm::ivec3> indices{ segments };

    positions[0] = glm::vec2(0.f);
    auto step    = static_cast<float>(2 * M_PI / segments);
    for (auto i = 0; i != segments; ++i)
    {
        auto x = radius * std::cos(i * step);
        auto y = radius * std::sin(i * step);

        positions[i + 1] = glm::vec2(x, y);
        indices[i]       = glm::ivec3(0, i + 1, (i != segments - 1) ? (i + 2) : 1);
    }

    return std::make_unique<Geometry>(positions, indices, Geometry::Mode::TRIANGLES_FAN);
};

std::unique_ptr<Geometry> RingGeometry(float innerRadius, float outerRadius, uint32_t segments)
{
    std::vector<glm::vec2>  positions{ segments * 2 };
    std::vector<glm::ivec3> indices{ segments * 2 };

    auto step = static_cast<float>(2 * M_PI / segments);
    for (auto i = 0; i != segments; ++i)
    {
        auto rx = std::cos(i * step);
        auto ry = std::sin(i * step);

        positions[i]            = glm::vec2(innerRadius * rx, innerRadius * ry);
        positions[segments + i] = glm::vec2(outerRadius * rx, outerRadius * ry);

        if (i == (segments - 1))
        {
            indices[i * 2]     = glm::ivec3(i, segments + i, segments);
            indices[i * 2 + 1] = glm::ivec3(i, segments, 0);
        }
        else
        {
            indices[i * 2]     = glm::ivec3(i, segments + i, segments + i + 1);
            indices[i * 2 + 1] = glm::ivec3(i + 1, segments + i + 1, i);
        }
    }

    return std::make_unique<Geometry>(positions, indices, Geometry::Mode::TRIANGLES);
};

} // namespace graphics
} // namespace klei
