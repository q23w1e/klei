#include "GLES3.hxx"
#include "Render.hxx"
#include "GLDebug.hxx"

#include <algorithm>
#include <sstream>

namespace klei
{
namespace graphics
{

std::string Render::Init()
{
    std::stringstream error;
    error << "ERROR::RENDER::";

    if (SDL_InitSubSystem(SDL_INIT_VIDEO) != 0)
    {
        error << "INIT_VIDEO_FAILED\n";
        error << SDL_GetError();

        return error.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

    //    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    //    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2);

    SDL_GL_SetSwapInterval(1);

    // should be public and configurable (at least for !ANDROID_OS)
    int         width  = 640;
    int         height = 480;
    std::string title  = "An Klei Space";

#ifdef __ANDROID__
    SDL_DisplayMode displayMode;
    if (SDL_GetCurrentDisplayMode(0, &displayMode) == 0)
    {
        width  = displayMode.w;
        height = displayMode.h;
    }
    mWindow = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height,
                               SDL_WINDOW_SHOWN);
#else
    mWindow = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height,
                               SDL_WINDOW_OPENGL);
#endif

    if (!mWindow)
    {
        error << "CREATE_WINDOW_FAILED\n";
        error << SDL_GetError();
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s", SDL_GetError());

        return error.str();
    }

    SDL_GLContext context = SDL_GL_CreateContext(mWindow);
    if (!context)
    {
        error << "CREATE_RENDER_CONTEXT_FAILED\n";
        error << SDL_GetError();

        return error.str();
    }

    // Initialize GLEW
#ifdef __ANDROID__
    if (!gladLoadGLES2Loader((GLADloadproc)SDL_GL_GetProcAddress))
    {
        error << "FAILED_TO_INIT_GLAD\n";
        error << SDL_GetError();

        return error.str();
    }
#else
    if (!gladLoadGLES2Loader((GLADloadproc)SDL_GL_GetProcAddress))
    //    if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
    {
        error << "FAILED_TO_INIT_GLAD\n";
        error << SDL_GetError();

        return error.str();
    }
#endif

    glEnable(GL_BLEND);
    GLDebug::CHECK_ERROR();
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    GLDebug::CHECK_ERROR();

    mClearColor  = glm::vec4(0.5f);
    mViewMatrix = glm::mat3(1.0f);
    updateProjectionMatrix();
    updateViewMatrix();

    return "";
}

void Render::Destroy()
{
    SDL_DestroyWindow(mWindow);
    SDL_GL_DeleteContext(mContext);
    SDL_QuitSubSystem(SDL_INIT_VIDEO);
}

void Render::ClearBuffer()
{
    glClearColor(static_cast<GLclampf>(mClearColor.r), static_cast<GLclampf>(mClearColor.g),
                 static_cast<GLclampf>(mClearColor.b), static_cast<GLclampf>(mClearColor.a));
    GLDebug::CHECK_ERROR();
    glClear(GL_COLOR_BUFFER_BIT);
    GLDebug::CHECK_ERROR();
}

void Render::SwapBuffers()
{
    SDL_GL_SwapWindow(mWindow);
}

void Render::AddToQueue(Drawable* drawable)
{
    auto iter = std::find_if(begin(mDrawables), end(mDrawables),
                             [&](Drawable* lhs) { return lhs->DrawOrder() > drawable->DrawOrder(); });
    if (iter != end(mDrawables))
    {
        mDrawables.insert(iter, drawable);
    }
    else
    {
        mDrawables.push_back(drawable);
    }
}

void Render::RemoveFromQueue(Drawable* drawable)
{
    if (auto iter = std::find(std::begin(mDrawables), std::end(mDrawables), drawable); iter != std::end(mDrawables))
    {
        mDrawables.erase(iter);
    }
}

void Render::Update()
{
    ClearBuffer();

    for (auto& drawable : mDrawables)
    {
        if (drawable->IsVisible())
        {
            drawable->Draw();
        }
    }

    SwapBuffers();
}

std::pair<int, int> Render::WindowSize()
{
    int width  = 0;
    int height = 0;

    if (mWindow)
    {
        SDL_GetWindowSize(mWindow, &width, &height);
    }

    return { width, height };
}

void Render::updateProjectionMatrix()
{
    auto [width, height]    = WindowSize();
    mProjectionMatrix[0][0] = 2.f / width;
    mProjectionMatrix[1][1] = 2.f / height;
}

void Render::updateViewMatrix()
{
    auto [width, height] = WindowSize();
    mViewMatrix[2][0]   = -width * 0.5f;
    mViewMatrix[2][1]   = -height * 0.5f;
}

} // namespace graphics
} // namespace klei
