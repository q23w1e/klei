#include <utility>

#include "MeshComponent.hxx"

namespace klei
{
namespace graphics
{

MeshComponent::MeshComponent(Element& element, std::unique_ptr<Geometry> geometry, std::unique_ptr<Style> style,
                             uint32_t drawOrder)
    : Component(element)
    , Drawable(drawOrder)
    , geometry{ std::move(geometry) }
    , style{ std::move(style) }
{
    Space::GetInstance().GetRender().AddToQueue(this);
}

MeshComponent::~MeshComponent()
{
    Space::GetInstance().GetRender().RemoveFromQueue(this);
}

void MeshComponent::Update(float deltaTime)
{
    style->UpdateMVPMatrix(mElement.TransformMatrix());
    style->UpdateUniforms();
    geometry->UpdateBuffersIfNeeded();
}

void MeshComponent::Draw()
{
    style->Use();
    geometry->DrawElements();
}

} // namespace graphics
} // namespace klei
