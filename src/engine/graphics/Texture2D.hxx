#pragma once

#include "GLDebug.hxx"

#include <utility>
#include <vector>

#include "GLES3.hxx"

namespace klei
{
namespace graphics
{

namespace Texture
{
enum class Format
{
    RGB  = GL_RGB,
    RGBA = GL_RGBA
};

enum class Filter
{
    NEAREST = GL_NEAREST,
    LINEAR  = GL_LINEAR
};

enum class MipmapFilter
{
    NEAREST = GL_LINEAR_MIPMAP_NEAREST,
    LINEAR  = GL_LINEAR_MIPMAP_LINEAR
};

} // namespace Texture

class Texture2D
{
public:
    Texture2D(const std::string& filename, Texture::Format format, bool flipped = true);

    long            Width() { return mWidth; }
    long            Height() { return mHeight; }
    bool            Flipped() { return mFlipped; }
    Texture::Format Format() { return mFormat; }

    void                                        Filter(Texture::Filter min, Texture::Filter mag);
    std::pair<Texture::Filter, Texture::Filter> Filter();
    void                                        MipmapFilter(Texture::MipmapFilter min, Texture::MipmapFilter mag);
    std::pair<Texture::MipmapFilter, Texture::MipmapFilter> MipmapFilter();

    void Bind();
    void UpdateParameters();

private:
    std::vector<unsigned char> loadFromFile(const std::string& filename);

    unsigned int          mId;
    unsigned long         mWidth;
    unsigned long         mHeight;
    bool                  mFlipped;
    Texture::Format       mFormat;
    Texture::Filter       mMinFilter       = Texture::Filter::LINEAR;
    Texture::Filter       mMagFilter       = Texture::Filter::LINEAR;
    Texture::MipmapFilter mMinMipmapFilter = Texture::MipmapFilter::LINEAR;
    Texture::MipmapFilter mMagMipmapFilter = Texture::MipmapFilter::LINEAR;
};

} // namespace graphics
} // namespace klei
