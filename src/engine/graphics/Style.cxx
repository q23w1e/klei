#include "Style.hxx"

namespace klei
{
namespace graphics
{

Style::Style(std::unique_ptr<Shader> shader)
    : mShader{ std::move(shader) }
    , mModelViewProjectionMatrix{ glm::mat3(1.0) }
{
}

void Style::UpdateUniforms()
{
    mShader->Use();

    mShader->UniformMatrix3fv("uModelViewProjectionMatrix", mModelViewProjectionMatrix);

    // TODO: refactor using std::visit
    for (const auto& [name, value] : mParameters)
    {
        if (std::holds_alternative<float>(value))
        {
            mShader->Uniform1f(name, std::get<float>(value));
        }
        else if (std::holds_alternative<glm::vec3>(value))
        {
            mShader->Uniform3f(name, std::get<glm::vec3>(value));
        }
        else if (std::holds_alternative<glm::vec4>(value))
        {
            mShader->Uniform4f(name, std::get<glm::vec4>(value));
        }
        else if (std::holds_alternative<glm::mat3>(value))
        {
            mShader->UniformMatrix3fv(name, std::get<glm::mat3>(value));
        }
    }
}

void Style::Use()
{
    mShader->Use();
}

void Style::UpdateMVPMatrix(const glm::mat3& model)
{
    auto& render = klei::Space::GetInstance().GetRender();

    mModelViewProjectionMatrix = render.ProjectionMatrix() * render.ViewMatrix() * model;
}

std::unique_ptr<Style> Styles::FlatColor()
{
    auto style = std::make_unique<Style>(klei::Storage::Shaders::FlatColor());

    style->SetParameter<glm::vec4>("uColor", glm::vec4(1.0));

    return style;
}

std::unique_ptr<Style> Styles::VertexColor()
{
    return std::make_unique<Style>(klei::Storage::Shaders::VertexColor());
}

} // namespace graphics
} // namespace klei
