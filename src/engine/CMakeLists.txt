cmake_minimum_required(VERSION 3.10)

set(SDL_SOURCES_DIR /home/q23w1e/CXX/libs/SDL2-2.0.8)
set(SDL_BUILD_DIR /home/q23w1e/CXX/libs/SDL2-2.0.8/build)

if(ANDROID)
    if (NOT SDL_SOURCES_DIR)
        message(FATAL_ERROR "SDL_SOURCES_DIR is not specified. Should be passed from gradle.build script")
    endif()

    if (NOT SDL_BUILD_DIR)
        message(FATAL_ERROR "SDL_BUILD_DIR is not specified. Should be passed from gradle.build script")
    endif()

    add_subdirectory(${SDL_SOURCES_DIR} ${SDL_BUILD_DIR})
else()
    find_package(SDL2 REQUIRED)
endif()

add_library(glm INTERFACE)
target_include_directories(glm INTERFACE libraries/glm/include)

add_library(glad SHARED libraries/glad/glad.c)
target_include_directories(glad PUBLIC libraries/glad)

file(GLOB_RECURSE KLEI_SOURCES *.cxx)
set(SOURCES ${SOURCES} ${KLEI_SOURCES})

add_library(klei SHARED ${SOURCES})

if(UNIX)
    if (ANDROID)
        target_link_libraries(klei
                SDL2
                glm
                glad
                log
                android
                EGL
                )
    else()
        target_link_libraries(klei
                SDL2
                glad
                glm
                GL
                )
    endif()
elseif (MINGW)
target_link_libraries(klei
            mingw32
            SDL2main
            SDL2
            -mwindows
            opengl32
            glew32
            -static-libstdc++
            -static-libgcc
            -lstdc++fs
            )
endif()

# for now public interface for games also includes headers with "only for internal usage" purpose (including libraries)=/
target_include_directories(klei PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
