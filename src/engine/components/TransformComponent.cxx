#include "TransformComponent.hxx"

namespace klei
{

TransformComponent::TransformComponent(Element& element, uint32_t updateOrder)
    : Component(element, updateOrder)
    , position{ glm::vec2{ 0.f } }
    , rotation{ 0.f }
    , scale{ 1.f }
    , mTransformMatrix{ glm::mat3(1.0f) }
{
}

void TransformComponent::Update(float deltaTime)
{
    updateTransformMatrix();
}

void TransformComponent::updateTransformMatrix()
{
    float cos = std::cos(rotation);
    float sin = std::sin(rotation);

    mTransformMatrix[0][0] = scale.x * cos;
    mTransformMatrix[1][0] = scale.x * -sin;
    mTransformMatrix[0][1] = scale.y * sin;
    mTransformMatrix[1][1] = scale.y * cos;
    mTransformMatrix[2][0] = position.x;
    mTransformMatrix[2][1] = position.y;
}

} // namespace klei
