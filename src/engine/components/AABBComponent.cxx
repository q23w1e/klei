#include "AABBComponent.hxx"
#include <graphics/Geometry.hxx>

AABBComponent::AABBComponent(klei::Element& element, float size, uint32_t updateOrder)
    : Component(element, updateOrder)
    , mSize{ size }
{
    updateBounds();
}

void AABBComponent::Update(float deltaTime)
{
    updateBounds();
}

bool AABBComponent::IsIntersect(const AABBComponent& other)
{
    // separating plane on X
    if (min.x > other.max.x || max.x < other.min.x)
    {
        return false;
    }
    // separating plane on Y
    if (min.y > other.max.y || max.y < other.min.y)
    {
        return false;
    }

    return true;
}

void AABBComponent::updateBounds()
{
    min = max = mElement.position;
    min -= mSize * 0.5f * mElement.scale;
    max += mSize * 0.5f * mElement.scale;
}

void AABBComponent::ForceUpdateBounds(const glm::vec2& position)
{
    min = max = position;
    min -= mSize * 0.5f * mElement.scale;
    max += mSize * 0.5f * mElement.scale;
}
