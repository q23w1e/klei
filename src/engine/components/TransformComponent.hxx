#pragma once

#include "core/Component.hxx"

namespace klei
{
class TransformComponent : public Component
{
    explicit TransformComponent(Element& element, uint32_t updateOrder = 100);

    void Update(float deltaTime) override;

    const glm::mat3& TransformMatrix() { return mTransformMatrix; };

    glm::vec2 position;
    float     rotation;
    glm::vec2 scale;

private:
    void updateTransformMatrix();

    glm::mat3 mTransformMatrix;
};

} // namespace klei
