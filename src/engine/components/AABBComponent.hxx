#pragma once

#include <core/Component.hxx>
#include <graphics/MeshComponent.hxx>

class AABBComponent : public klei::Component
{
public:
    AABBComponent(klei::Element &element, float size, uint32_t updateOrder = 90);
    glm::vec2 min;
    glm::vec2 max;

    void Update(float deltaTime) override;
    void ForceUpdateBounds(const glm::vec2& position);
    bool IsIntersect(const AABBComponent& other);

private:
    void updateBounds();
    const float mSize;
};
