#pragma once

#include <random>

namespace klei
{

class Random
{
public:
    static float FloatRange(float min, float max);
    static int IntRange(int min, int max);

    template <typename T>
    static T Range(T min, T max)
    {
        std::uniform_real_distribution<T> dist{min, max};

        return dist(getGenerator());
    };

private:
    static std::mt19937& getGenerator();
};

}; // namespace klei
