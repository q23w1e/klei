#pragma once

#include "../common/ABC.hxx"

#include <ostream>
#include <sstream>
#include <string>

#include <glm/vec2.hpp>

namespace klei
{
namespace Gamepad
{

enum class Button
{
    UP,
    DOWN,
    LEFT,
    RIGHT,
    A,
    B,
    START,
    STOP
};
}

struct Touch
{
    enum class Phase
    {
        Began,
        Moved,
        Ended,
    };

    Phase     phase         = Phase::Ended;
    glm::vec2 position      = glm::vec2(0);
    glm::vec2 positionDelta = glm::vec2(0);
    float     pressure      = 0;

    void Log()
    {
        std::stringstream ss;
        // clang-format off
        switch (phase)
        {
            case Touch::Phase::Began: ss << "TYPE: " << "SDL_FINGERDOWN" << " | "; break;
            case Touch::Phase::Moved: ss << "TYPE: " << "SDL_FINGERMOTION" << " | "; break;
            case Touch::Phase::Ended: ss << "TYPE: " << "SDL_FINGERUP" << " | "; break;
        }
        // clang-format on
        ss << "POSITION: " << position.x << ", " << position.y << " | ";
        ss << "POSITION_DELTA: " << positionDelta.x << ", " << positionDelta.y << " | ";
        ss << "PRESSURE: " << pressure << ", ";

//        SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, ss.str().c_str());
    }

};

class Input final : public Subsystem
{
public:
    std::string Init() override;
    void        Destroy() override;

    void ProcessInput();
    bool QuitSignal();

    bool IsButtonPressed(const std::string& name);
    bool IsButtonPressed(Gamepad::Button button);
    bool IsButtonReleased(const std::string& name);
    bool IsButtonReleased(Gamepad::Button button);

    const Touch& GetTouch() const;

private:
    Touch mTouch;
    bool  mQuitRequested = false;
};

} // namespace klei
