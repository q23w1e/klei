#pragma once

#include "Component.hxx"
#include "Space.hxx"

#include <vector>

#include <glm/mat3x3.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace klei
{

class Space;
class Component;

class Element
{
public:
    enum class State
    {
        Active,
        Inactive,
        Destroyed,
    };

    Element();
    virtual ~Element();

    virtual void Update(float deltaTime){};
    virtual void Destroy();

    void AddComponent(Component* component);
    void RemoveComponent(Component* component);

    const glm::mat3& TransformMatrix() { return mTransformMatrix; };

    State     state;
    glm::vec2 position;
    float     rotation;
    glm::vec2 scale;

    template <typename T>
    T* GetComponentByType()
    {
        for (const auto& component : mComponents)
        {
            if (auto casted = dynamic_cast<T*>(component))
            {
                return casted;
            }
        }

        return nullptr;
    }

    // in order to have access to globalUpdate() method;
    friend class Space;

private:
    void updateComponents(float deltaTime);
    void globalUpdate(float deltaTime);
    void updateTransformMatrix();

    std::vector<Component*> mComponents;
    glm::mat3               mTransformMatrix;
};

} // namespace klei
