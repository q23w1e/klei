#include "Component.hxx"

namespace klei
{

Component::Component(Element &element, uint32_t updateOrder)
    : mElement{ element }
    , mUpdateOrder{ updateOrder }
{
    mElement.AddComponent(this);
}

Component::~Component() {
    std::cout << "component dctor called" << std::endl;
//    mElement.RemoveComponent(this);
}

} // namespace klei
