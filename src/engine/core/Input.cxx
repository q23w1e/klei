#include "Input.hxx"
#include "Space.hxx"

#include <bitset>
#include <iostream>
#include <map>
#include <sstream>

#ifdef __ANDROID__
#include <SDL.h>
#include <SDL_keycode.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_keycode.h>
#endif

namespace klei
{

namespace Gamepad
{

struct Key
{
    std::string name;
    SDL_Keycode keyCode;
    uint8_t     offset;
};

const std::map<Button, Key> Buttons = {
    { Button::UP, Key{ "UP", SDLK_UP, 0 } },
    { Button::DOWN, Key{ "DOWN", SDLK_DOWN, 2 } },
    { Button::LEFT, Key{ "LEFT", SDLK_LEFT, 4 } },
    { Button::RIGHT, Key{ "RIGHT", SDLK_RIGHT, 6 } },
};

const std::map<SDL_Keycode, Button> ButtonKeycodes = {
    { SDLK_UP, Button::UP },
    { SDLK_DOWN, Button::DOWN },
    { SDLK_LEFT, Button::LEFT },
    { SDLK_RIGHT, Button::RIGHT },
};

const std::map<std::string, Button> ButtonNames = {
    { "UP", Button::UP },
    { "DOWN", Button::DOWN },
    { "LEFT", Button::LEFT },
    { "RIGHT", Button::RIGHT },
};

// each consequence pair of bits represent gamepad button statuses during one loop iteration:
// bitset[0] - is button pressed, bitset[1] - is button released.
std::bitset<16> State;

}; // namespace Gamepad

std::string Input::Init()
{
    if (IsInitialized())
    {
        std::cout << "Input subsystem already initialized. Skipped" << std::endl;
        return "";
    }

    std::stringstream error;
    error << "ERROR::INPUT::";

    if (SDL_InitSubSystem(SDL_INIT_EVENTS) != 0)
    {
        error << "INIT_FAILED\n";
        error << SDL_GetError();

        return error.str();
    }

    mInitialized = true;

    return "";
}

void Input::Destroy()
{
    SDL_QuitSubSystem(SDL_INIT_EVENTS);
}

void Log(SDL_TouchFingerEvent& event)
{
    std::stringstream ss;
    // clang-format off
    switch (event.type)
    {
        case SDL_FINGERDOWN: ss << "TYPE: " << "SDL_FINGERDOWN" << " | "; break;
        case SDL_FINGERMOTION: ss << "TYPE: " << "SDL_FINGERMOTION" << " | "; break;
        case SDL_FINGERUP: ss << "TYPE: " << "SDL_FINGERUP" << " | "; break;
        default: ss << "UNDEFINED" << " | "; break;
    }
    // clang-format on
    ss << "POSITION: " << event.x << ", " << event.y << " | ";
    ss << "POSITION_DELTA: " << event.dx << ", " << event.dy << " | ";
    ss << "PRESSURE: " << event.pressure << ", ";

    SDL_Log("%s", ss.str().c_str());
}
void Input::ProcessInput()
{
    Gamepad::State.reset();

    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_QUIT:
            {
                mQuitRequested = true;
                break;
            }
            case SDL_KEYDOWN:
            {
                SDL_Keycode keycode = event.key.keysym.sym;
                if (Gamepad::ButtonKeycodes.count(keycode))
                {
                    Gamepad::State[Gamepad::Buttons.at(Gamepad::ButtonKeycodes.at(keycode)).offset] = true;
                }
                break;
            }
            case SDL_KEYUP:
            {
                SDL_Keycode keycode = event.key.keysym.sym;
                if (Gamepad::ButtonKeycodes.count(keycode))
                {
                    Gamepad::State[Gamepad::Buttons.at(Gamepad::ButtonKeycodes.at(keycode)).offset + 1] = true;
                }
                break;
            }
            case SDL_FINGERDOWN:
            {
                mTouch.phase         = Touch::Phase::Began;
                mTouch.position      = glm::vec2(event.tfinger.x, event.tfinger.y);
                mTouch.positionDelta = glm::vec2(0);
                mTouch.pressure      = event.tfinger.pressure;
                Log(event.tfinger);
                break;
            }
            case SDL_FINGERMOTION:
            {
                mTouch.phase         = Touch::Phase::Moved;
                mTouch.position      = glm::vec2(event.tfinger.x, event.tfinger.y);
                mTouch.positionDelta = glm::vec2(event.tfinger.dx, event.tfinger.dy);
                mTouch.pressure      = event.tfinger.pressure;
                Log(event.tfinger);
                break;
            }
            case SDL_FINGERUP:
            {
                mTouch.phase         = Touch::Phase::Ended;
                mTouch.position      = glm::vec2(event.tfinger.x, event.tfinger.y);
                mTouch.positionDelta = glm::vec2(0);
                mTouch.pressure      = event.tfinger.pressure;
                Log(event.tfinger);
                break;
            }
            default:
                break;
        }
    }
}

bool Input::QuitSignal()
{
    return mQuitRequested;
}

bool Input::IsButtonPressed(Gamepad::Button button)
{
    return Gamepad::State[Gamepad::Buttons.at(button).offset];
}

bool Input::IsButtonReleased(Gamepad::Button button)
{
    return Gamepad::State[Gamepad::Buttons.at(button).offset + 1];
}

bool Input::IsButtonPressed(const std::string& name)
{
    if (Gamepad::ButtonNames.count(name))
    {
        return Gamepad::State[Gamepad::Buttons.at(Gamepad::ButtonNames.at(name)).offset];
    }

    return false;
}

bool Input::IsButtonReleased(const std::string& name)
{
    if (Gamepad::ButtonNames.count(name))
    {
        return Gamepad::State[Gamepad::Buttons.at(Gamepad::ButtonNames.at(name)).offset + 1];
    }

    return false;
}

const Touch& Input::GetTouch() const
{
    return mTouch;
}

} // namespace klei
