#include "Space.hxx"

#include <algorithm>
#include <iostream>
#include <sstream>

namespace klei
{

using namespace std;

std::string Space::Expand()
{
    if (mInitialized)
    {
        std::cout << "Already initialized. Skipped" << std::endl;

        return "";
    }

    mRender = std::make_unique<graphics::Render>();
    if (auto failed = mRender->Init(); !failed.empty())
    {
        Collapse();
        return failed;
    }
    mSubsystems.push_back(mRender.get());

    mInput = std::make_unique<Input>();
    if (auto failed = mInput->Init(); !failed.empty())
    {
        Collapse();
        return failed;
    }
    mSubsystems.push_back(mRender.get());

    mAudio = std::make_unique<audio::SoundSystem>();
    if (auto failed = mAudio->Init(); !failed.empty())
    {
        Collapse();
        return failed;
    }
    mSubsystems.push_back(mRender.get());

    mStorage = std::make_unique<Storage>();
    if (auto failed = mStorage->Init(); !failed.empty())
    {
        Collapse();
        return failed;
    }
    mSubsystems.push_back(mStorage.get());

    mClock = std::make_unique<Clock>();

    mInitialized = true;

    return "";
}

void Space::TickTack()
{
    bool running = true;
    while (running)
    {
        mClock->Tick();

        mInput->ProcessInput();
        running = !mInput->QuitSignal();

        if (!mClock->IsPaused())
        {
            float deltaTime = mClock->DeltaTime();
            Update(deltaTime);
        }

        mRender->Update();
    }
}

void Space::Collapse()
{
    for (auto& subsystem : mSubsystems)
    {
        if (subsystem)
        {
            subsystem->Destroy();
        }
    }
    // let be paranoid
    SDL_Quit();
}

void Space::Update(float deltaTime)
{
    mUpdating = true;
    std::vector<Element*> destroyingElements;
    for (auto& element : mElements)
    {
        if (element->state == Element::State::Active)
        {
            element->globalUpdate(deltaTime);
        }

        if (element->state == Element::State::Destroyed)
        {
            destroyingElements.emplace_back(element);
        }
    }

    // destroy all marked for destroy elements
    std::for_each(std::begin(destroyingElements), std::end(destroyingElements), [](Element* elem) { delete elem; });

    std::copy(std::begin(mPendingElements), std::end(mPendingElements), std::back_inserter(mElements));
    mPendingElements.clear();

    mUpdating = false;
}

void Space::AddElement(Element* element)
{
    if (mUpdating)
    {
        mPendingElements.push_back(element);
    }
    else
    {
        mElements.push_back(element);
    }
}

void Space::RemoveElement(Element* element)
{
    if (auto iter = find(begin(mPendingElements), end(mPendingElements), element); iter != end(mPendingElements))
    {
        iter_swap(iter, end(mPendingElements) - 1);
        mPendingElements.pop_back();
    }

    if (auto iter = find(begin(mElements), end(mElements), element); iter != end(mElements))
    {
        iter_swap(iter, end(mElements) - 1);
        mElements.pop_back();
    }
}

} // namespace klei
