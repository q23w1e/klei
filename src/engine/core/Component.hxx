#pragma once

#include "Element.hxx"

#include <cstdint>

namespace klei
{

class Element;

class Component
{
public:
    explicit Component(Element& element, uint32_t updateOrder = 100);
    virtual ~Component();

    virtual void Update(float deltaTime) = 0;
    int          GetUpdateOrder() { return mUpdateOrder; };

protected:
    Element&  mElement;
    uint32_t mUpdateOrder;
};

} // namespace klei
