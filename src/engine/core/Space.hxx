#pragma once

#include "../audio/SoundSystem.hxx"
#include "../common/ABC.hxx"
#include "../common/Storage.hxx"
#include "../graphics/Render.hxx"
#include "Clock.hxx"
#include "Element.hxx"
#include "Input.hxx"

#include <cassert>
#include <memory>
#include <string>
#include <vector>

#ifdef __ANDROID__
#include <SDL.h>
#include <SDL_video.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#endif

namespace klei
{
class Element;

class Space: public Singleton<Space>
{
public:
    std::string   Expand();
    void          TickTack();
    void          Collapse();

    audio::SoundSystem& GetAudio() { return *mAudio; }
    Input&              GetInput() { return *mInput; }
    graphics::Render&   GetRender() { return *mRender; }
    Storage&            GetResources() { return *mStorage; }
    Clock&              GetClock() { return *mClock; }

    void Update(float deltaTime);
    void AddElement(Element* element);
    void RemoveElement(Element* element);

    template <class T>
    T* GetElementByType()
    {
        T* found = nullptr;

        for (const auto& element : mElements)
        {
            if (auto casted = dynamic_cast<T*>(element))
            {
                found = casted;
            }
        }

        for (const auto& element : mPendingElements)
        {
            if (auto casted = dynamic_cast<T*>(element))
            {
                found = casted;
            }
        }

        return found;
    }

private:
    bool mInitialized = false;

    std::unique_ptr<graphics::Render>   mRender;
    std::unique_ptr<Input>              mInput;
    std::unique_ptr<audio::SoundSystem> mAudio;
    std::unique_ptr<Storage>            mStorage;
    std::vector<Subsystem*>             mSubsystems;

    std::unique_ptr<Clock> mClock;

    // TODO: rewrite using smart pointers
    std::vector<Element*> mElements;
    std::vector<Element*> mPendingElements;
    bool                  mUpdating = false;
};

} // namespace klei
