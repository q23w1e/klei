#pragma once

#ifdef __ANDROID__
#include "SDL.h"
#else
#include "SDL2/SDL.h"
#endif

namespace klei
{

class Clock
{
public:
    Clock();

    void Tick();

    float    DeltaTime() const { return mDeltaTime * mTimeScale; }
    float    ElapsedTimeFromStart() const { return (mTicksCount - mOnCreateTicksCount) / 1000.f; }
    uint32_t FrameCount() const { return mFrameCount; }

    bool IsPaused() const { return mPaused; }
    void Paused(bool state) { mPaused = state; }

    void  TimeScale(float scale) { mTimeScale = scale; }
    float TimeScale() const { return mTimeScale; }

private:
    bool     mPaused;
    uint32_t mOnCreateTicksCount;
    uint64_t mTicksCount;
    uint32_t mFrameCount;
    float    mDeltaTime;
    float    mMaxDelta;
    float    mTimeScale;
};

} // namespace klei
