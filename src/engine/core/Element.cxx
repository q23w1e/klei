#include "Element.hxx"
#include <algorithm>

namespace klei
{

using namespace std;

Element::Element()
    : state{ State::Active }
    , position{ glm::vec2{ 0.f } }
    , rotation{ 0.f }
    , scale{ 1.f }
    , mTransformMatrix{ glm::mat3(1.0f) }
{
    Space::GetInstance().AddElement(this);
}

Element::~Element()
{
    Space::GetInstance().RemoveElement(this);

    // delete all components (look towards smart pointers)
    std::for_each(std::begin(mComponents), std::end(mComponents), [](Component* component) { delete component; });
    mComponents.clear();
}

void Element::AddComponent(Component* component)
{
    auto iter = find_if(begin(mComponents), end(mComponents),
                        [&](Component* other) { return other->GetUpdateOrder() > component->GetUpdateOrder(); });
    if (iter != end(mComponents))
    {
        mComponents.insert(iter, component);
    }
    else
    {
        mComponents.push_back(component);
    }
}

void Element::RemoveComponent(Component* component)
{
    if (auto iter = find(begin(mComponents), end(mComponents), component); iter != end(mComponents))
    {
        mComponents.erase(iter);
    }
}

void Element::updateComponents(float deltaTime)
{
    for (auto& component : mComponents)
    {
        component->Update(deltaTime);
    }
}

void Element::globalUpdate(float deltaTime)
{
    updateTransformMatrix();

    updateComponents(deltaTime);
    Update(deltaTime);
}

void Element::updateTransformMatrix()
{
    float cos = std::cos(rotation);
    float sin = std::sin(rotation);

    mTransformMatrix[0][0] = scale.x * cos;
    mTransformMatrix[1][0] = scale.x * -sin;
    mTransformMatrix[0][1] = scale.y * sin;
    mTransformMatrix[1][1] = scale.y * cos;
    mTransformMatrix[2][0] = position.x;
    mTransformMatrix[2][1] = position.y;
}

void Element::Destroy()
{
    state = State::Destroyed;
}

} // namespace klei
