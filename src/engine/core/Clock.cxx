#include "Clock.hxx"

#include <iostream>

namespace klei
{

Clock::Clock()
    : mPaused{ false }
    , mMaxDelta{ 0.05f }
    , mTimeScale{ 1.0f }
{
    mOnCreateTicksCount = SDL_GetTicks();
    mTicksCount         = mOnCreateTicksCount;
}

void Clock::Tick()
{
    if (!mPaused)
    {
        while (!SDL_TICKS_PASSED(SDL_GetTicks(), mTicksCount + 16))
            ;

        mDeltaTime = (SDL_GetTicks() - mTicksCount) / 1000.0f;
        if (mDeltaTime > mMaxDelta)
        {
            mDeltaTime = mMaxDelta;
        }

        mTicksCount = SDL_GetTicks();
        ++mFrameCount;
    }
}

} // namespace klei
