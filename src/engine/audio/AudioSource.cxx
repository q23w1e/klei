#include "AudioSource.hxx"
#include "../core/Space.hxx"

namespace klei
{
namespace audio
{

AudioSource::AudioSource(const std::string& file)
{
    // TODO: implement a storage for downloaded audio files
    if (SDL_LoadWAV(file.data(), &mSpec, &mBuffer, &mLength) == nullptr)
    {
        std::cerr << "ERROR::AUDIO::LOAD_FILE_FAILED" << std::endl;
        std::cerr << SDL_GetError() << std::endl;
    }
}

AudioSource::~AudioSource()
{
    SDL_FreeWAV(mBuffer);
}

void AudioSource::Play(bool looping)
{
    mPlaybackBuffer = mBuffer;
    mPlaybackLength = mLength;

    this->mLooping = looping;
    mPlaying       = true;

    // register audio source in audio system
    auto& audio = klei::Space::GetInstance().GetAudio();
    audio.AddSound(this);
}

void AudioSource::QueueData(const Device& device, uint32_t chunkSize)
{
    uint32_t len = (chunkSize > mPlaybackLength ? mPlaybackLength : chunkSize);

    SDL_QueueAudio(device.Id(), mPlaybackBuffer, len);

    mPlaybackBuffer += len;
    mPlaybackLength -= len;

    if (mPlaybackLength == 0)
    {
        if (mLooping)
        {
            //            mPlaybackBuffer = mSource->Buffer();
            //            mPlaybackLength = mSource->Length();
        }
        else
        {
            mPlaying = false;
        }
    }
}

} // namespace audio
} // namespace klei
