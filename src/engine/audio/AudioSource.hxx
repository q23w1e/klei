#pragma once

#include "Device.hxx"

#include <string>

namespace klei
{
namespace audio
{

class SoundSystem;

class AudioSource
{
public:
    explicit AudioSource(const std::string& file);
    ~AudioSource();

    void Play(bool looping);
    void Pause(bool paused) { mPlaying = !paused; }

    bool IsPlaying() const { return mPlaying; }
    bool IsLooping() const { return mLooping; }

    void QueueData(const Device& device, uint32_t chunkSize);

    friend class SoundSystem;

private:
    //    SoundSystem& mSoundSystem;
    //    std::shared_ptr<Source> mSource;

    SDL_AudioSpec mSpec;
    uint32_t      mLength;
    uint8_t*       mBuffer;

    uint8_t* mPlaybackBuffer;
    uint32_t mPlaybackLength;
    uint16_t mChunkSize;

    bool mLooping;
    bool mPlaying;
};

}; // namespace audio
} // namespace klei
