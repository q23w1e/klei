#include "Device.hxx"

#include <sstream>
#include <stdexcept>

/* SDL_AudioFormat of files, such as s16 little endian */
#define AUDIO_FORMAT AUDIO_S16LSB

/* Frequency of the file */
#define AUDIO_FREQUENCY 48000

/* 1 mono, 2 stereo, 4 quad, 6 (5.1) */
#define AUDIO_CHANNELS 2

/* Specifies a unit of audio data to be used at a time. Must be a power of 2 */
#define AUDIO_SAMPLES 4096

namespace klei
{
namespace audio
{

void Device::Pause()
{
    if (enabled)
    {
        SDL_PauseAudioDevice(mId, 1);
        return;
    }
}

void Device::Unpause()
{
    if (enabled)
    {
        SDL_PauseAudioDevice(mId, 0);
        return;
    }
}

void Device::Lock()
{
    if (enabled)
    {
        SDL_LockAudioDevice(mId);
        return;
    }
}

void Device::Unlock()
{
    if (enabled)
    {
        SDL_UnlockAudioDevice(mId);
        return;
    }
}

bool Device::Open(const SDL_AudioSpec& spec)
{
    SDL_AudioSpec obtained;
    mId = SDL_OpenAudioDevice(nullptr, 0, &spec, &obtained, SDL_AUDIO_ALLOW_ANY_CHANGE);
    if (mId == 0)
    {
        return false;
    }
    enabled = true;
    Unpause();

    return true;
}

SDL_AudioDeviceID Device::Id() const
{
    return mId;
}

const SDL_AudioSpec& Device::Spec() const
{
    return mSpec;
}

} // namespace audio
} // namespace klei
