#include "SoundSystem.hxx"

#include <iostream>
#include <sstream>
#include <vector>

#include "SDLAudio.hxx"

namespace klei
{
namespace audio
{

static const auto kAudioFormat   = AUDIO_S16LSB;
static const auto kAudioFreq     = 48000;
static const auto kAudioChannels = 2;
static const auto kAudioSamples  = 4096;
static const auto kMaxSounds     = 10;

void SoundSystem::AudioCallback(void* soundSystemPtr, uint8_t* stream, int streamLength)
{
    std::fill_n(stream, streamLength, '\0');
    std::vector<std::vector<AudioSource*>::iterator> endedSounds;

    auto soundSystem = static_cast<SoundSystem*>(soundSystemPtr);

    for (auto iter = std::begin(soundSystem->mSounds); iter < std::end(soundSystem->mSounds); ++iter)
    {
        auto sound = *iter;

        if (!sound->mPlaying)
        {
            break;
        }

        uint32_t len = ((streamLength > sound->mPlaybackLength) ? sound->mPlaybackLength : streamLength);
        SDL_MixAudioFormat(stream, sound->mPlaybackBuffer, soundSystem->mPlaybackSpec.format, len, SDL_MIX_MAXVOLUME);

        sound->mPlaybackLength -= len;
        sound->mPlaybackBuffer += len;

        if (sound->mPlaybackLength == 0)
        {
            if (sound->mLooping)
            {
                sound->mPlaybackLength = sound->mLength;
                sound->mPlaybackBuffer = sound->mBuffer;
            }
            else
            {
                sound->mPlaying = false;
                endedSounds.push_back(iter);
            }
        }
    }

    for (auto& sound : endedSounds)
    {
        soundSystem->mSounds.erase(sound);
    }
}

std::string SoundSystem::Init()
{
    std::stringstream error;

    mPlaybackSpec.freq     = kAudioFreq;
    mPlaybackSpec.channels = kAudioChannels;
    mPlaybackSpec.format   = kAudioFormat;
    mPlaybackSpec.samples  = kAudioSamples;
    mPlaybackSpec.callback = AudioCallback;
    mPlaybackSpec.userdata = this;

    if (SDL_Init(SDL_INIT_AUDIO) != 0)
    {
        error << "ERROR::SDL::INIT_AUDIO_FAILED\n";
        error << SDL_GetError();

        return error.str();
    }

    mPlaybackDevice = std::make_unique<Device>();
    if (!mPlaybackDevice->Open(mPlaybackSpec))
    {
        error << "ERROR::SDL::OPEN_AUDIO_DEVICE_FAILED\n";
        error << SDL_GetError();

        return error.str();
    }

    return "";
}

// void SoundSystem::AddSound(std::shared_ptr<AudioSource> sound)
//{
//    mSounds.push_back(sound);
//}

void SoundSystem::AddSound(AudioSource* sound)
{
    mSounds.push_back(sound);
}

// void SoundSystem::UpdateBuffersIfNeeded()
//{
//    uint32_t chunkSize = mPlaybackSpec.samples * mPlaybackSpec.format * mPlaybackSpec.channels;
//    chunkSize /= mSounds.size();
//    for (auto& sound : mSounds)
//    {
//        if (sound->IsPlaying())
//        {
//            sound->QueueData(*mPlaybackDevice, chunkSize);
//        }
//        // do something
//    }
//}

} // namespace audio
} // namespace klei
