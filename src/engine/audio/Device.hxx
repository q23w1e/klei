#pragma once

#include "SDLAudio.hxx"

namespace klei
{
namespace audio
{

class Device
{
public:
    bool Open(const SDL_AudioSpec& spec);

    void Pause();
    void Unpause();

    void Lock();
    void Unlock();

    SDL_AudioDeviceID    Id() const;
    const SDL_AudioSpec& Spec() const;

private:
    SDL_AudioDeviceID mId;
    SDL_AudioSpec     mSpec;
    bool              enabled;
};

} // namespace audio
} // namespace klei
