#pragma once

#include "../common/ABC.hxx"
#include "AudioSource.hxx"
#include "Device.hxx"

#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <vector>

namespace klei
{
namespace audio
{

class AudioSource;

class SoundSystem : public Subsystem
{
public:
    std::string Init() override;
    void        Destroy() override{};

    const Device&        GetPlaybackDevice() const { return *mPlaybackDevice; }
    const SDL_AudioSpec& GetPlaybackSpec() const { return mPlaybackSpec; }
    void                 AddSound(AudioSource* sound);

private:
    static void AudioCallback(void* soundSystemPtr, uint8_t* stream, int streamLength);

    SDL_AudioSpec           mPlaybackSpec;
    std::unique_ptr<Device> mPlaybackDevice;
    std::unique_ptr<Device> mRecordDevice;

    //    std::list<std::shared_ptr<AudioSource>> mSounds;
    std::vector<AudioSource*> mSounds;
    //    uint8_t                                 mMaxSounds;
};

} // namespace audio
} // namespace klei
