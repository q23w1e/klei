#pragma once

#include <iostream>
#include <memory>
#include <string>

template <typename T>
class Singleton
{
public:
    static T& GetInstance();

    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton) = delete;

protected:
    Singleton() = default;
};

template <typename T>
T& Singleton<T>::GetInstance()
{
    static T instance;

    return instance;
}

class Subsystem
{
public:
    virtual ~Subsystem() = default;

    virtual std::string Init()    = 0;
    virtual void        Destroy() = 0;
    virtual bool        IsInitialized() { return mInitialized; };

protected:
    bool mInitialized = false;
};

class Updatable
{
public:
    virtual ~Updatable() = default;

    virtual void Update() = 0;
};

class Drawable
{
public:
    explicit Drawable(uint32_t order = 100)
        : mVisible{ true }
        , mDrawOrder{ order } {};
    virtual ~Drawable() = default;

    virtual void Draw() = 0;

    bool IsVisible() { return mVisible; }
    void Visibility(bool state) { mVisible = state; }

    uint32_t DrawOrder() const { return mDrawOrder; }

private:
    bool     mVisible;
    uint32_t mDrawOrder;
};