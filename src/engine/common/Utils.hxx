#pragma once

namespace klei
{

template <typename E>
constexpr auto GetUnderlyingType(E e) -> typename std::underlying_type<E>::type
{
    return static_cast<typename std::underlying_type<E>::type>(e);
}

} // namespace klei
