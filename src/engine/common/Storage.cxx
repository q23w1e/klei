#include "Storage.hxx"

namespace klei
{

std::unique_ptr<graphics::Shader> Storage::Shaders::FlatColor()
{
    return std::make_unique<graphics::Shader>("shaders/colored.vert", "shaders/colored.frag");
    return std::make_unique<graphics::Shader>("../assets/shaders/colored.vert", "../assets/shaders/colored.frag");
}

std::unique_ptr<graphics::Shader> Storage::Shaders::VertexColor()
{
//    return std::make_unique<graphics::Shader>("../assets/shaders/vertexColored.vert",
//                                              "../assets/shaders/vertexColored.frag");
    return std::make_unique<graphics::Shader>("shaders/vertexColored.vert",
                                              "shaders/vertexColored.frag");
}

std::string Storage::Init()
{
    // here we can check if assets folder available, for example
    LoadResources();
    return "";
}

void Storage::LoadResources()
{
    // file path can be stored and read from config file
}

} // namespace klei
