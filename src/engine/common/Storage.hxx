#pragma once

#include "ABC.hxx"
#include "../graphics/Shader.hxx"

#include <memory>

namespace klei
{

class Storage : public Subsystem
{
public:
    std::string Init() override;
    void        LoadResources();
    void        Destroy() override {};

    class Shaders
    {
    public:
        static std::unique_ptr<graphics::Shader> FlatColor();
        static std::unique_ptr<graphics::Shader> VertexColor();
        static std::unique_ptr<graphics::Shader> Textured() {};
    };
};

} // namespace klei
