#include "Random.hxx"

namespace klei
{

float Random::FloatRange(float min, float max)
{
    std::uniform_real_distribution<float> dist{ min, max };

    return dist(getGenerator());
}

int Random::IntRange(int min, int max)
{
    std::uniform_int_distribution dist{ min, max };

    return dist(getGenerator());
}

std::mt19937& Random::getGenerator()
{
    std::random_device rd;

    static std::mt19937 generator;
    generator.seed(rd());

    return generator;
}

} // namespace klei
