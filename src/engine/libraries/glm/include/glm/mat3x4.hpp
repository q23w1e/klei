/// @ref core
/// @file glm/mat3x4.hpp

#pragma once
#include "libraries/glm/include/glm/ext/matrix_double3x4.hpp"
#include "libraries/glm/include/glm/ext/matrix_double3x4_precision.hpp"
#include "libraries/glm/include/glm/ext/matrix_float3x4.hpp"
#include "libraries/glm/include/glm/ext/matrix_float3x4_precision.hpp"
