/// @ref core
/// @file glm/mat4x4.hpp

#pragma once
#include "libraries/glm/include/glm/ext/matrix_double4x4.hpp"
#include "libraries/glm/include/glm/ext/matrix_double4x4_precision.hpp"
#include "libraries/glm/include/glm/ext/matrix_float4x4.hpp"
#include "libraries/glm/include/glm/ext/matrix_float4x4_precision.hpp"

