/// @ref core
/// @file glm/mat2x4.hpp

#pragma once
#include "libraries/glm/include/glm/ext/matrix_double2x4.hpp"
#include "libraries/glm/include/glm/ext/matrix_double2x4_precision.hpp"
#include "libraries/glm/include/glm/ext/matrix_float2x4.hpp"
#include "libraries/glm/include/glm/ext/matrix_float2x4_precision.hpp"

