/// @ref core
/// @file glm/mat2x3.hpp

#pragma once
#include "libraries/glm/include/glm/ext/matrix_double2x3.hpp"
#include "libraries/glm/include/glm/ext/matrix_double2x3_precision.hpp"
#include "libraries/glm/include/glm/ext/matrix_float2x3.hpp"
#include "libraries/glm/include/glm/ext/matrix_float2x3_precision.hpp"

