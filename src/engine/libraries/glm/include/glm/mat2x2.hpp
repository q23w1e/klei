/// @ref core
/// @file glm/mat2x2.hpp

#pragma once
#include "libraries/glm/include/glm/ext/matrix_double2x2.hpp"
#include "libraries/glm/include/glm/ext/matrix_double2x2_precision.hpp"
#include "libraries/glm/include/glm/ext/matrix_float2x2.hpp"
#include "libraries/glm/include/glm/ext/matrix_float2x2_precision.hpp"

