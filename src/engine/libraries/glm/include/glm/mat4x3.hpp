/// @ref core
/// @file glm/mat4x3.hpp

#pragma once
#include "libraries/glm/include/glm/ext/matrix_double4x3.hpp"
#include "libraries/glm/include/glm/ext/matrix_double4x3_precision.hpp"
#include "libraries/glm/include/glm/ext/matrix_float4x3.hpp"
#include "libraries/glm/include/glm/ext/matrix_float4x3_precision.hpp"
