/// @ref core
/// @file glm/mat3x3.hpp

#pragma once
#include "libraries/glm/include/glm/ext/matrix_double3x3.hpp"
#include "libraries/glm/include/glm/ext/matrix_double3x3_precision.hpp"
#include "libraries/glm/include/glm/ext/matrix_float3x3.hpp"
#include "libraries/glm/include/glm/ext/matrix_float3x3_precision.hpp"
