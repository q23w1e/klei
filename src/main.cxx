#include "game/Game.hxx"

int main(int argc, char* argv[])
{
    auto& space = klei::Space::GetInstance();

    std::string fail = space.Expand();
    if (!fail.empty())
    {
        std::cerr << "Can not initialize Space. Game terminated" << std::endl;
        std::cerr << fail << std::endl;

        return EXIT_FAILURE;
    }

    auto& game = Game::GetInstance();
    game.Init();

    space.TickTack();

    return EXIT_SUCCESS;
};