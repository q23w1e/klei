#version 300 es

layout(location = 0) in vec2 aPos;
layout(location = 1) in vec3 aColor;
layout(location = 2) in vec2 aTexCoord;

uniform float uTime;

out vec3 vertColor;
out vec2 texCoord;

void main()
{
    float t = sin(uTime) * 0.5 + 0.5;
    gl_Position = vec4(aPos, 0.0, 1.0);
    vertColor = aColor;
    texCoord = aTexCoord * t;
}
