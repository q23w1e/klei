#version 330 core

layout(location = 0) in vec2 aPos;
layout(location = 1) in vec3 aColor;

uniform mat3 uProjection;
uniform mat3 uTransform;

out vec3 vertColour;

void main()
{
    vec3 position = uProjection * uTransform * vec3(aPos, 1.0);
    gl_Position   = vec4(position, 1.0);
}