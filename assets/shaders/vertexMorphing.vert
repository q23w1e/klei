#version 300 es

layout(location = 0) in vec2 aPos;
layout(location = 1) in vec3 aColor;
layout(location = 2) in vec2 aTargetPos;

uniform float uTime;

out vec3 vertColor;

void main()
{
    float t = sin(uTime) * 0.5 + 0.5;
    gl_Position = vec4(mix(aPos, aTargetPos, t), 0.0, 1.0);
    vertColor =  vec3(1.0, 1.0, 1.0) * t;
}
