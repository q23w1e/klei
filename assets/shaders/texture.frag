#version 300 es

precision mediump float;

in vec3 vertColor;
in vec2 texCoord;

uniform sampler2D ourTexture;

out vec4 fragColor;

void main()
{
    fragColor = texture(ourTexture, texCoord);
//   fragColor = vec4(vertColor, 1.0);
//    fragColor = vec4(0.5, 0.5, 0.5, 1.0);

}
