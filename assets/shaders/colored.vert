#version 300 es

layout(location = 0) in vec2 aPos;

uniform mat3 uModelViewProjectionMatrix;

void main()
{
    vec3 position = uModelViewProjectionMatrix * vec3(aPos, 1.0);
    gl_Position   = vec4(position, 1.0);
}
