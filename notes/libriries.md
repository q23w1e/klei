### Static Libraries
Simply a collection of ordinary object files.  

Conventionally, ends with `.a` suffix (`.lib` on Windows)   
A program using static library takes copies of the code that it uses from the static library and makes it a part of the 
program.  
Permit users to link program without recompiling. 
(saving recompilation time... but recompilation time is less important given today's faster compilers)  
Useful if you wish to permit programmers to link to you library, but do not want to give the library source code
(which is advantage to the library vendor, but not to a programmer trying to use the library)  

```ar rcs my_library.a objfile.o objfile2.o```

### Shared Libraries
Loaded by programs when they start.

- reduce the amount of code that is duplicated in each program, keeping the binaries small.
- allow to replace the shared object with one that is functionally equivalent, w/o needed needing to recompile the program
- can be loaded to the application at runtime (the general mechanism for implementing binary plug-ins)
- have a small additional cost for the execution of the functions as well as a run-time loading cost

----
- Be careful about the order of the parameters when using gcc; 
  the -l option is a linker option, and thus needs to be placed AFTER the name of the file to be compiled. 
  This is quite different from the normal option syntax.
