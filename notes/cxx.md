#### Translation unit

Basic _unit_ of compilaiton in C++. It consists of the content of a single source file, plus the contents of any header files directly or indirectly included by it, minus those lines that we were ignored using conditional preprocessing statement.

A single *translation unit* can be compiled into object file, library or executable program.

An object file contains not only the compiled machine code for all functions defined in the .cpp file, but also all of its global and static variables and, in addition, _unresolved references_ to functions and global variables defined elsewhere.


#### Linkage
Every definition has a _linkage_ property. 

Definition with **external** _property_ is visible to and can be referenced by translation units other than the one in which it appears.  
Definition with **internal** _property_ can only be "seen" inside the translation unit in which it appears and thus cannot be referenced by other translation units.

In a sense, linkage is the translation unit's equivalent of the _public_ and _private_ in C++ class
definition.


#### static keyword

