**How to check if library is installed?**
```
ldconfig -p | grep libname
```
**How to install package dependencies only?**
```
sudo apt-get install $(apt-cache depends <PACKAGE> | grep Depends | sed "s/.*ends:\ //" | tr '\n' ' ')
```




